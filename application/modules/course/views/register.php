<style>

</style>
<section class="section blog-article">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-ms-12">
                    <div class="blog-posts">
                        <div class="title">
                            <h3>ลงทะเบียนเรียน</h3>
                            <!-- <div class="separator"></div> -->
                        </div>
                        <div>
                           <table id="customers" >
							  <tr>
							    <th colspan="2"  >รายละเอียดคอร์สเรียน</th>
							    <th style="text-align: center;width: 10%">ราคาปกติ</th>
							    <th style="text-align: center;width: 10%">โปรโมโชั่น</th>
							    <th style="text-align: center;width: 10%">คูปองส่วนลด</th>
							    <th style="text-align: center;width: 10%">ราคารวม</th>
							  </tr>
							  <tr>
							    <td  style="width: 150px">
							    	
							    	    <img src="<?php echo $info['image'];?>" style="width: 150px">
								 </td>
								 <td  style="width: 50%"> 
								
								    	<h4><?php echo $info['title'] ?></h4>
								    	<p><?php echo $info['excerpt'] ?></p>
								   
							    </td>
							    <td style="text-align: right;">
							    	
							    	<?php if(!empty($info['promotion']) && $info['promotion']['discount']!='0'){ ?>
							    	   <span style="text-decoration: line-through;"><?php echo number_format($info['price'])?></span>
							    	<?php }else{ ?>
							    	   <?php echo number_format($info['price'])?>
							    	<?php } ?>
							    </td>
							   
							    	<?php if(!empty($info['promotion']) && $info['promotion']['discount']!='0'){ ?>
							    		 <td style="text-align: right;">
							    		<?php echo number_format($info['promotion']['discount'])?>
							    		 </td>
							    	<?php }else if(!empty($info['promotion']) && $info['promotion']['discount']=='0'){ ?>
							    		 <td style="text-align: center;">
								    		โปรโมชั่นแถมคอร์ส
								    	</td>
							    	<?php }else{ ?>
							    		 <td style="text-align: center;">
								    		ไม่มีโปรโมชั่น
								    	</td>
							    	<?php } ?>
							   
							    <td style="text-align: right;">
							    	<input type="text" name="couponCode" id="couponCode" class="form-control">
							    </td>
							    <td style="text-align: right;">
							    	<?php if(!empty($info['promotion']) && $info['promotion']['discount']!='0'){ ?>
							    		<input type="hidden" name="price" id="price" value="<?php echo $info['promotion']['discount']; ?>">
							    		<?php echo number_format($info['promotion']['discount'])?>
							    	<?php }else{ ?>
							    		<input type="hidden" name="price" id="price" value="<?php echo $info['price']; ?>">
							    		<?php echo number_format($info['price'])?>
							    	<?php } ?>
							    </td>
							  </tr>

							  <?php if(!empty($promotionContent)){ 
							  	foreach ($promotionContent as $key => $value) {
							   ?>

							  	<tr>
								    <td  style="width: 150px">
								    	
								    	    <img src="<?php echo $value->image;?>" style="width: 150px">
									 </td>
									 <td  style="width: 50%"> 
									
									    	<h4><?php echo $value->title ?></h4>
									    	<p><?php echo $value->excerpt ?></p>
									   
								    </td>
								    <td style="text-align: center;">
								    	
								    	แถมฟรี
								    </td>
								   <td style="text-align: center;">
								    	
								    	-
								    </td>
								    	
								   
								    <td style="text-align: center;">
								    	-
								    </td>
								    <td style="text-align: center;">
								    	-
								    </td>
								  </tr>

							  <?php } } ?>
							 <!--  <tr>
							  	<td colspan="6" style="text-align: center;">
							  		
							  	</td>
							  </tr> -->
							  
							</table>
							<center><input type="hidden" name="courseId" id="courseId" value="<?php echo $info['courseId']; ?>">
							  		<input type="hidden" name="promotionId" id="promotionId" value="<?php echo $info['promotion']['promotionId']; ?>">

							  		<div class="register-form">
							  	    <div id="form-success-div" class="text-success"></div> 
                                    <div id="form-error-div" class="text-danger"></div>
									 <a  class="button-click-2 registerSave"><span id="form-img-div"></span>  ยืนยันการลงทะเบียน</a>
									</div>
							</center>
							
                        </div>
                           
                    </div><!-- blog-posts -->
                </div><!-- col-lg-4 -->
                <div class="clearfix">
                	
                </div>
                
            </div>  
        </div>
    </section><!-- section -->

	<section class="section blog-article">
	        <div class="container">
	            <div class="row">
	                <div class="col-xl-6 col-lg-6 col-md-12 col-ms-12">
	                    <div class="blog-posts">
	                        <div class="title">
	                            <h3>วิธีการชำระเงิน</h3>
	                            <!-- <div class="separator"></div> -->
	                        </div>
	                        <div>
	                           <div class="payment">
												<?php echo html_entity_decode($payment); ?>
											</div>
								
	                        </div>
	                           
	                    </div><!-- blog-posts -->
	                </div><!-- col-lg-4 -->
	                <div class="col-xl-6 col-lg-6 col-md-12 col-ms-12">
	                    <div class="blog-posts">
	                        <div class="title">
	                            <h3>ติดต่อสอบถาม</h3>
	                           <!--  <div class="separator"></div> -->
	                        </div>
	                        <div class="register-social">
						
						<!-- <a target="_blank" href="https://www.facebook.com/<?=$facebook;?>">
						 <img style="margin-bottom:6px;" src="<?php echo base_url("assets/website/images/facebook-3.png") ?>" alt="Line " ><span><h4> &nbsp;&nbsp;<?=$facebook;?></h4></span>
						</a>
						<br> -->
						<!-- <a href="javascript:void(0)" onclick="window.open('http://line.me/ti/p/~<?=$idLine;?>', '_blank');">
						  <img style="margin-bottom:6px;" src="<?php echo base_url("assets/website/images/line-3.png") ?>" alt="Line " ><span><h4> &nbsp;&nbsp;<?=$idLine;?></h4></span>
						</a> -->
						<a href="https://m.me/<?php echo $facebook; ?>"><img src="<?php echo base_url("assets/website/images/messenger.png") ?>" style="width: 60px;"><span><h4>&nbsp;&nbsp;<?php echo $facebook; ?></h4></span></a>
						<br>
						<a href="tel:<?php echo $phoneNumber; ?>" style="padding-top: 10px;">
						   <img style="margin-bottom:6px;" src="<?php echo base_url("assets/website/images/phone-3.png") ?>" alt="Line " ><span><h4>&nbsp;&nbsp;<?php echo $phoneNumber; ?> </h4></span>
						</a>
						
				</div>
	                           
	                    </div><!-- blog-posts -->
	                </div><!-- col-lg-4 -->
	                
	            </div>  
	        </div>
	    </section><!-- section -->