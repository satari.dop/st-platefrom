<style>

</style>
<section class="section blog-article">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-ms-12">
                    <div class="blog-posts">
                        <div class="title">
                            <h3>รายการสั่งซื้อ</h3>
                        </div>
                        <?php if(!empty($info)){?>
                        
                       
                        	 <?php foreach ($info as $key => $value) { ?>
                        	<div class="row">
                        		
								<div class="col-lg-4 col-md-5 col-ms-12 ">
									<div class="content-activity">
		                        		<h5 style="font-weight: 600;">เลขที่อ้าอิง : <?php echo $value->code;?></h5>
		                        	</div>
									<div class="hover13 column">
										<a href="<?php echo site_url("course/detail/{$value->course['linkId']}");?>">
										<figure>
											
							    	    <img src="<?php echo $value->image;?>" style="width: 100%">
							    	
										</figure>
									    </a>
									</div>
								</div>
								<div class="col-lg-8 col-md-7 col-ms-12 ">
									<div class="content-activity">
										<h4 ><a href="<?php echo site_url("course/detail/{$value->course['linkId']}");?>"><?php echo $value->course['title'];?></a></h4>
										<p><?php echo $value->course['excerpt'];?></p>
									</div>
									<div class="content-activity">
										<h5 >วันที่ลงทะเบียน : <?php echo $value->createDate;?></h5>
										<h5 >ราคา : <?php echo number_format($value->price);?></h5>
										<h5 >ส่วนลด :  <?php if(!empty($value->couponCode)){ 
								   		if($value->type==2){
								   			$rt=($value->price*$value->discount)/100;
								   			$total=$value->price-$rt;
								   			echo $value->discount."%";
								   		}else{
								   			$total=$value->price-$value->discount;
								   			echo number_format($value->discount);
								   		}
								   		}else{
										   	$total=$value->price;
										   	echo "ไม่มีส่วนลด";
										 } 
								   	?></h5>
										<h5 >ยอดรวม : <?php echo number_format($total);?></h5>
										<h5 >สถานะ : <?php if($value->status=='0'){ echo "<font color=red>ยังไม่ชำระเงิน</font>"; }else{ echo "<font color=green>ชำระเงินเรียบร้อย</font>"; }?>
										</h5>
									</div>
								</div>
								


							</div><!-- row -->
							 <?php } ?>

                       
	                    <?php }else{ ?>

	                    	<center><h3>ไม่มีรายการสั่งซื้อ</h3></center>

	                    <?php } ?>
                           
                    </div><!-- blog-posts -->
                </div><!-- col-lg-4 -->
                <div class="clearfix">
                	
                </div>
                
            </div>  
        </div>
    </section><!-- section -->

	<section class="section blog-article">
	        <div class="container">
	            <div class="row">
	                <div class="col-xl-6 col-lg-6 col-md-12 col-ms-12">
	                    <div class="blog-posts">
	                        <div class="title">
	                            <h3>วิธีการชำระเงิน</h3>
	                            <!-- <div class="separator"></div> -->
	                        </div>
	                        <div>
	                           <div class="payment">
												<?php echo html_entity_decode($payment); ?>
											</div>
								
	                        </div>
	                           
	                    </div><!-- blog-posts -->
	                </div><!-- col-lg-4 -->
	                <div class="col-xl-6 col-lg-6 col-md-12 col-ms-12">
	                    <div class="blog-posts">
	                        <div class="title">
	                            <h3>ติดต่อสอบถาม</h3>
	                            <!-- <div class="separator"></div> -->
	                        </div>
	                        <div>
	                        	
	                        	<!-- <div class="floating-chat2 enter2">
									<a href="https://m.me/gorradesign" target="_blank" class="chat_a">
										<i class="fa fa-comments chat" aria-hidden="true"></i>
										<div class="chat">
											<div class="header">

											</div>
										</div>
									</a>
								</div> -->
								<div>
									<!-- <a href="javascript:void(0)" onclick="window.open('http://line.me/ti/p/~<?=$idLine;?>', '_blank');"><img src="<?php echo base_url("assets/website/images/line@.png") ?>" style="width: 250px;"></a> -->
									<a href="https://m.me/<?php echo $facebook; ?>"><img src="<?php echo base_url("assets/website/images/messenger.png") ?>" style="width: 60px;"> <?php echo $facebook; ?></a>
								</div>

	                           <div style="padding-top: 10px;">
									<!-- <a href="javascript:void(0)" onclick="window.open('http://line.me/ti/p/~<?=$idLine;?>', '_blank');"><img src="<?php echo base_url("assets/website/images/line@.png") ?>" style="width: 250px;"></a> -->
									<a href="tel:<?php echo $phoneNumber; ?>"><img src="<?php echo base_url("assets/website/images/Call.png") ?>" style="width: 60px;"> โทร : <?php echo $phoneNumber; ?></a>
								</div>
								
	                        </div>
	                           
	                    </div><!-- blog-posts -->
	                </div><!-- col-lg-4 -->
	                
	            </div>  
	        </div>
	    </section><!-- section -->