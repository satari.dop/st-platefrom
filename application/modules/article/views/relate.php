                    <div class="widget recent-posts">
						<h4 class="widget-title">LATEST</h4>
						 <div class="separator"></div>
						<!-- Recent Post Widget Starts -->
						<?php foreach ($info as $key => $rs) { ?>
					
							<div class="posts-thumb"> 
								<a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>">
									<img alt="img" src="<?php echo $rs['image']; ?>">
									<div class="text-block">
									<div class="text-2">
									    <h4><?php echo $rs['title'] ?></h4>
									    <p>หมวดหมู่ : <?php echo $rs['name'] ?></p>
									    <p><?php echo $rs['createDate'] ?></p>
								    </div>
								  </div>
								</a>
							</div>
							
							<div class="clearfix"></div>
						
					<?php } ?>
						<!-- Recent Post Widget Ends -->

						
						<!-- Recent Post Widget Ends -->
						</ul>
					</div>
					<!-- Latest Posts Widget Ends -->