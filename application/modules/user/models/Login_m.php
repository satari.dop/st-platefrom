<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_m Extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function by_email($email)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('member a')
                        ->where('a.email', $email)
                        ->get()
                        ->row_array();
        return $query; 
    }
    
    public function by_username($username)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('member a')
                        ->where('a.memberUsername', $username)
                        ->get()
                        ->row_array();
        return $query; 
    }
    
    public function update($value, $id)
    {
        $query = $this->db
                        ->where('memberId', $id)
                        ->update('member', $value);
        return $query;
    }

     public function get_by_email($param)
    {
        $query = $this->db
                        ->select('a.*')
                        ->select('b.active policyActive')
                        ->from('user a')
                        ->join('policy b', 'a.policyId = b.policyId', 'left')
                        ->where('a.email', $param['email'])
                        ->where('a.recycle', 0)
                        ->get()
                        ->row_array();
        return $query;                
    }

     public function get_by_username($param)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('user a')
                        ->where('a.username', $param['username'])
                        ->where('a.recycle', 0)
                        ->get()
                        ->row_array();
        return $query;                
    }
     public function get_by_oauth_uid($param)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('user a')
                        ->where('a.oauth_uid', $param['oauth_uid'])
                        ->where('a.recycle', 0)
                        ->get()
                        ->row_array();
        return $query;                
    }
    
    public function update_last_login()
    {
        $this->db->where('userId', $this->session->member['userId'])
                ->update('user', array('lastLogin'=>db_datetime_now(),'user_datetime_using'=>db_datetime_now(),'user_login_status'=>1,'session_id'=>$this->session->session_id ));
    }  

     public function update_last_login_2()
    {
        $this->db->where('userId', $this->session->member['userId'])
                ->update('user', array('user_datetime_using'=>db_datetime_now(),'user_login_status'=>1 ));
    }       

}

