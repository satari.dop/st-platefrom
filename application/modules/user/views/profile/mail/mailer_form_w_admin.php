<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>สถาบันออนไลน์</title>
<style type="text/css">
  #customers {
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #6d6e72;
    color: white;
}
</style>
</head>

<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left"  style="border: 2px solid #27160E;">
      <img src="<?php echo base_url("assets/website/") ?>images/logo.jpg" class="ls-bg" alt="" style="width: 45px;"/>
    </td>
  </tr>

  <tr>
    <td align="center" valign="top" bgcolor="" style="border: 2px solid #27160E;font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
        <tr>
          <td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#525252;">

          <div style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#000000;">คุณ <?php echo $withdraw_data->firstname.' '.$withdraw_data->lastname; ?></div>
            <div style="font-size:18px;">
                ได้ทำการถอนเงิน <b>เลขที่อ้างอิง : <?php echo  $withdraw_data->code ?></b>

                
            </div>

            
            </td>
        </tr>

        
      </table>
      </td>
  </tr>


  <tr>

    <td align="left" valign="top" bgcolor="#27160E" style="background-color:#27160E;color:#FFF;">
    <table width="100%" border="0" cellspacing="0" cellpadding="15">
      <tr>
        <td align="left" valign="top" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:13px;line-height: 20px;text-align: center;"> สถาบันออนไลน์
          </td>
      </tr>
    </table></td>
  </tr>

  <!-- EN Template -->
   


</table>
</body>
</html>
