<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Course_content_m Extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_rows($param) {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $this->db->order_by('order', 'asc')
                ->order_by('parentId', 'asc');        
        
        $query = $this->db
                        ->select('a.*')
                        ->from('course_content a')
                        ->get();
        return $query;
    }
    
    public function get_count($param){
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('course_content a')
                        ->get();
        return $query->num_rows();
    }
    
    private function _condition($param){

        
        if ( isset($param['email']) ) 
            $this->db->where('a.email', $param['email']);

        if ( isset($param['courseId']) ) 
            $this->db->where('a.courseId', $param['courseId']);
        
        if ( isset($param['course_contentId']) ) 
            $this->db->where('a.course_contentId', $param['course_contentId']);
        
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        
        if ( $this->router->method == 'order' ) {
            $this->db->where('active', 1);
        }
    }
    
    public function get_by_id($id)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('course_content a')
                        ->where('a.course_contentId', $id)
                        ->get()
                        ->row_array();
        return $query; 
    }
    
    public function row_by_class($class) {
        
        $query = $this->db
                        ->select('*')
                        ->from('course_content a')
                        ->where('class', $class)
                        ->get();
        return $query->row();
    }    
    
    public function row_by_class_method($class, $method) {
        
        $query = $this->db
                        ->select('*')
                        ->from('course_content a')
                        ->where('class', $class)
                        ->or_like('class', $class)
                        ->get();
        return $query->row();
    }
    
    public function get_course_content($courseId)
    {
        $this->db
                ->order_by('parentId', 'asc')
                ->order_by('order', 'asc')
                ->order_by('course_contentId', 'asc'); 
        $query = $this->db
                        ->from('course_content a')
                        ->select('a.course_contentId, a.title, a.parentId, a.order')
                        ->where('a.recycle', 0)
                        ->where('a.courseId', $courseId)
                        ->get()
                        ->result_array();
        return $query;
    }
    
    public function get_course_content_order()
    {
        $query = $this->db
                        ->from('course_content a')
                        ->select('a.course_contentId, a.title, a.parentId, a.order')
                        ->where('a.status', 1)
                        ->where('a.recycle', 0)
                        ->order_by('a.order')
                        ->order_by('a.parentId')
                        ->get()
                        ->result_array();
        return $query;
    }
    
    public function insert($value)
    {
        $this->db->insert('course_content', $value);
        return $this->db->insert_id();
    }
    
    public function delete($param)
    {
        $rs = $this->db
                    ->where_in('course_contentId', $param)
                    ->delete('course_content');
        return $rs;
    }
    
    public function update($value, $id)
    {
        $rs = $this->db
                    ->where('course_contentId', $id)
                    ->update('course_content', $value);
        return $rs;        
    }  
    
    public function update_order($value)
    {
        $rs = $this->db
                    ->update_batch('course_content', $value, 'course_contentId');
        return $rs;   
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('course_contentId', $id)
                        ->update('course_content', $value);
        return $query;
    }
    
}

