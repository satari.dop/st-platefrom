<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member_m extends MY_Model {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('user a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('user a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) {
         // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.username', $param['keyword'])
                    ->or_like('a.firstname', $param['keyword'])
                    ->or_like('a.lastname', $param['keyword'])
                    ->or_like('a.email', $param['keyword'])
                    ->or_like('a.phone', $param['keyword'])
                     ->or_like('a.couponCode', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.createDate,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updateDate,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter

        if ( isset($param['search']['value']) ) {
            $this->db
                    ->group_start()
                    ->like('a.username', $param['search']['value'])
                    ->or_like('a.firstname', $param['search']['value'])
                    ->or_like('a.lastname', $param['search']['value'])
                    ->or_like('a.email', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.firstname";
            // if ($param['order'][0]['column'] == 2) $columnOrder = "a.username";
            // if ($param['order'][0]['column'] == 3) $columnOrder = "a.phone";
            // if ($param['order'][0]['column'] == 4) $columnOrder = "a.email";
             if ($param['order'][0]['column'] == 2) $columnOrder = "a.lastLogin";
            if ($param['order'][0]['column'] == 3) $columnOrder = "a.createDate";
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        }else{
            $this->db->order_by('a.createDate', 'desc');
        }
        
        if ( isset($param['email']) ) 
            $this->db->where('a.email', $param['email']);
        
        if ( isset($param['userId']) ) 
            $this->db->where('a.userId', $param['userId']);
        if ( !in_array($this->router->method, array("profile","check_password","check_email")))
            $this->db->where('a.userId !=', $this->session->user['userId']);
        if ( isset($param['recycle']) ) {
            if (is_array($param['recycle'])) {
                $this->db->where_in('a.recycle', $param['recycle']);
            } else {
                $this->db->where('a.recycle', $param['recycle']);
            }
        }
        if ( $this->session->user['type'] != 'developer')
            $this->db->where('a.type !=', 'developer');
       //if ( $this->session->user['type'] != 'admin')
            $this->db->where('a.type !=', 'admin');

       if ( isset($param['type']) ) 
            $this->db->where('a.type', $param['type']);
    }
    
    public function insert($value) {
        $this->db->insert('user', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('userId', $id)
                        ->update('user', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('userId', $id)
                        ->update('user', $value);
        return $query;
    }
    
    public function update_last_login()
    {
        $this->db->where('userId', $this->session->user['userId'])
                ->update('user', array('lastLogin'=>db_datetime_now()));
    }   
    
}
