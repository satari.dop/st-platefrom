<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Course_member_m Extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_rows($param) {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
           
        
        $query = $this->db
                        ->select('a.*')
                        ->select('b.title')
                        ->select('c.firstname,c.lastname,c.email,c.phone,c.lastLogin')
                        ->join('course b', 'a.courseId = b.courseId', 'left')
                        ->join('user c', 'a.userId = c.userId', 'left')
                        ->from('course_member a')
                        ->get();
        return $query;
    }
    
    public function get_count($param){
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->select('b.title')
                        ->select('c.firstname,c.lastname,c.email,c.phone,c.lastLogin')
                        ->join('course b', 'a.courseId = b.courseId', 'left')
                        ->join('user c', 'a.userId = c.userId', 'left')
                        ->from('course_member a')
                        ->get();
        return $query->num_rows();
    }
    
    private function _condition($param){

        //$this->db->where('c.recycle', 0);
        //$this->db->where('c.active', 1);

        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('c.firstname', $param['keyword'])
                    ->or_like('c.lastname', $param['keyword'])
                    ->or_like('c.email', $param['keyword'])
                    ->or_like('c.phone', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(c.lastLogin,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "c.firstname";
                        
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 4) $columnOrder = "c.lastLogin";
                if ($param['order'][0]['column'] == 5) $columnOrder = "a.createDate";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycleDate";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
               
        // END form filter


        if ( isset($param['email']) ) 
            $this->db->where('a.email', $param['email']);

        if ( isset($param['courseId']) ) 
            $this->db->where('a.courseId', $param['courseId']);
        
        if ( isset($param['course_memberId']) ) 
            $this->db->where('a.course_memberId', $param['course_memberId']);
        
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

         if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        } 

        //$this->db->order_by('a.createDate', 'desc');
        
        // if ( $this->router->method == 'order' ) {
        //     $this->db->where('active', 1);
        // }
    }
    
    public function get_by_id($id)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('course_member a')
                        ->where('a.course_memberId', $id)
                        ->get()
                        ->row_array();
        return $query; 
    }
    
    public function row_by_class($class) {
        
        $query = $this->db
                        ->select('*')
                        ->from('course_member a')
                        ->where('class', $class)
                        ->get();
        return $query->row();
    }    
    
    public function row_by_class_method($class, $method) {
        
        $query = $this->db
                        ->select('*')
                        ->from('course_member a')
                        ->where('class', $class)
                        ->or_like('class', $class)
                        ->get();
        return $query->row();
    }
    
    public function get_course_member($courseId)
    {
        $this->db
                ->order_by('parentId', 'asc')
                ->order_by('order', 'asc')
                ->order_by('course_memberId', 'asc'); 
        $query = $this->db
                        ->from('course_member a')
                        ->select('a.course_memberId, a.title, a.parentId, a.order')
                        ->where('a.recycle', 0)
                        ->where('a.courseId', $courseId)
                        ->get()
                        ->result_array();
        return $query;
    }
    
    public function get_course_member_order()
    {
        $query = $this->db
                        ->from('course_member a')
                        ->select('a.course_memberId, a.title, a.parentId, a.order')
                        ->where('a.status', 1)
                        ->where('a.recycle', 0)
                        ->order_by('a.order')
                        ->order_by('a.parentId')
                        ->get()
                        ->result_array();
        return $query;
    }
    
    public function insert($value)
    {
        $this->db->insert('course_member', $value);
        return $this->db->insert_id();
    }
    
    public function delete($param)
    {
        $rs = $this->db
                    ->where_in('course_memberId', $param)
                    ->delete('course_member');
        return $rs;
    }
    
    public function update($value, $id)
    {
        $rs = $this->db
                    ->where('course_memberId', $id)
                    ->update('course_member', $value);
        return $rs;        
    }  
    
    public function update_order($value)
    {
        $rs = $this->db
                    ->update_batch('course_member', $value, 'course_memberId');
        return $rs;   
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('course_memberId', $id)
                        ->update('course_member', $value);
        return $query;
    }
    
}

