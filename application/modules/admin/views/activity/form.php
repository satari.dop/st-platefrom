<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-create', 'method' => 'post')) ?>
    <div class="box-body">    
        <div class="form-group">
            <div class="col-sm-7 col-sm-offset-2">
                <label class="icheck-inline"><input type="checkbox" name="recommend" value="1" class="icheck" <?php echo isset($info->recommend) && $info->recommend == 1 ? "checked" : NULL ?>/> รายการแนะนำ</label>
            </div>
        </div>  
        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">รูปภาพหน้าปก</label>
            <div class="col-sm-7">
                <div class="row">
                    <!--  -->
                    <div class="col-sm-4" style="margin-bottom: 10px">
                         <!-- image-preview-filename input [CUT FROM HERE]-->
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename" disabled="disabled" name="fileName"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                    <span class="glyphicon glyphicon-remove"></span> ยกเลิก
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title">อัพโหลด</span>
                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                                </div>
                            </span>


                        </div>
                       
                       <!-- /input-group image-preview [TO HERE]--> 
                    </div>
                    <div class="col-sm-8">
                         <span><!-- ไฟล์ภาพ : jpg , jpeg , png , gif / --> 1110px × 624px (กว้าง x สูง) </span>
                    </div>

                    <div class="col-sm-12" id="cover-image" style="padding-left: 0px;">
                        <script> var dataCoverImage = <?php echo isset($coverImage) ? json_encode($coverImage) : "{}" ?> </script>
                    </div>
                    
                     
                </div>
                
                
            </div>
        </div>
        <div class="form-group" style="display: none;">
            <label class="col-sm-2 control-label" for="title">หมวดหมู่</label>
            <div class="col-sm-7">
                <?php 

                echo form_dropdown('categoryId', $categoryDropDown, isset($info->categoryId) ? $info->categoryId : 26 , 'class="form-control select2" required') 
                ?>
            </div>
        </div> 
        <div class="form-group">
            <label class="col-sm-2 control-label">ชื่อ</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="excerpt">เนื้อหาย่อ</label>
            <div class="col-sm-7">
                <textarea name="excerpt" rows="3" class="form-control"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
            </div>
        </div>    

        <div class="form-group">
            <label class="col-sm-2 control-label" for="descript">เนื้อหา</label>
            <div class="col-sm-7">
                <?php echo $this->ckeditor->editor("detail", isset($info->detail) ? $info->detail : NULL, "normal", 200); ?>
            </div>
        </div> 
        <div class="form-group">
            <label class="col-sm-2 control-label">จำนวนที่นั้งเหลือ</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->userNum) ? $info->userNum : 0 ?>" type="text" id="input-userNum" class="form-control" name="userNum" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">ราคา</label>
            <div class="col-sm-3">
                   <input value="<?php echo isset($info->price) ? number_format($info->price) : NULL ?>" type="text" class="form-control amount" name="price" required>
            </div>
             <label class="col-sm-1 control-label">ราคาโปรโมชั่น</label>
            <div class="col-sm-3">
                  <input value="<?php echo !empty($info->promotion) ? number_format($info->promotion) : NULL ?>" type="text" class="form-control amount" name="promotion" >
            </div>
        </div> 
        <div class="form-group">

            <label class="col-sm-2 control-label">ช่วงวันที่จัดกิจกรรม</label>

            <div class="col-sm-3">

               <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>"/>

                    <input type="hidden" name="startDate"  value="<?php echo isset($info->startDate) ? $info->startDate : NULL ?>"/>

                    <input type="hidden" name="endDate"  value="<?php echo isset($info->endDate) ? $info->endDate : NULL ?>"/>

            </div>

        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">เวลา</label>
            <div class="col-sm-3">
                   <select class="form-control select2" name="startTime" id="startTime" class="startTime">
                   
                   <?php foreach ($time as $key => $_time) { ?>
                     <option value="<?=$_time?>" <?php if($_time.':00'==$info->startTime){ ?> selected <?php } ?>><?=$_time?></option>
                   <?php } ?>
 
                   </select>
            </div>
             <label class="col-sm-1 control-label">ถึง</label>
            <div class="col-sm-3">
                  <select class="form-control select2" name="endTime"  id="endTime"  class="endTime">
                  <option value="" selected><?php echo $this->lang->line('booking_end_time');?></option>
                    <?php foreach ($time as $key => $_time) { ?>
                     <option value="<?=$_time?>" <?php if($_time.':00'==$info->endTime){ ?> selected <?php } ?>><?=$_time?></option>
                   <?php } ?>
                 </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">สถานที่</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->location) ? $info->location : NULL ?>" type="text" id="input-location" class="form-control" name="location" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">หัวข้อแกลอรี่</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->title_gallery) ? $info->title_gallery : NULL ?>" type="text" id="input-location" class="form-control" name="title_gallery" >
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">รูปภาพแกลอรี่</label>
            <div class="col-sm-7">
                <div class="row">
                    <!--  -->
                    <div class="col-sm-4" style="margin-bottom: 10px">
                         <button onclick="set_container($('#gallery-image'), 'multiple', 'tmpl-gallery-image')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                    </div>
                    <div class="col-sm-8">
                         
                    </div>

                 
                     <div class="col-sm-12"  id="gallery-image" style="padding-left: 0px;">
                                <script> var dataGalleryImage = <?php echo isset($galleryImage) ? json_encode($galleryImage) : "{}" ?></script>
                     </div>
                    
                     
                </div>
                
                
            </div>
        </div>

        <h4 class="block">ข้อมูล SEO</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >ชื่อเรื่อง (Title)</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->metaTitle) ? $info->metaTitle : NULL ?>" type="text" id="" class="form-control" name="metaTitle">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">คำอธิบาย (Description)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo isset($info->metaDescription) ? $info->metaDescription : NULL ?></textarea>
            </div>
        </div>   
        <div class="form-group">
            <label class="col-sm-2 control-label">คำหลัก (Keyword)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo isset($info->metaKeyword) ? $info->metaKeyword : NULL ?></textarea>
            </div>
        </div> 

        
    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->activityId) ? encode_id($info->activityId) : 0 ?>">
    <?php echo form_close() ?>
</div>
<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>

<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
       
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
           
        </div>
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-content-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
        <input type="hidden" id="uploadId" name="contentImageId" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="contentImageTitle" placeholder="ชื่อภาพ">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
            <button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-gallery-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
        <input type="hidden" id="uploadId" name="galleryImageId[]" value="{%=obj.uploadId%}">
    </div>
    <div class="col-sm-3" id="action">
        <div class="btn-group">
           
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
   <div class="clearfix"></div> 
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-doc-attach">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 10px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-8" id="info">
        <input type="hidden" id="uploadId" name="docAttachId[]" value="{%=obj.uploadId%}">
        <input value="{%=obj.title%}" type="text" class="form-control" name="docAttachTitle[]" placeholder="ชื่อไฟล์">
    </div>
    <div class="col-sm-2" id="action">
        <div class="btn-group">
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบไฟล์</button>
        </div>
    </div>
    <div class="clearfix"></div>
    {% } %}
</script>

