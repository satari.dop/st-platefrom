<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">สอนสดในองค์กร</h3>
    </div>
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">
        
        <div class="form-group">
            <label class="col-sm-2 control-label" >รายละเอียด</label>
            <div class="col-sm-7">
                <?php echo $this->ckeditor->editor("organization", isset($info['organization']) ? $info['organization'] : NULL, "normal", 500); ?>
            </div>
        </div>
        
        
        
    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <?php echo form_close() ?>
</div>