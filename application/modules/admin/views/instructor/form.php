<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">    
        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">รูปภาพ</label>
            <div class="col-sm-7">
                <div class="row">
                    <!--  -->
                    <div class="col-sm-4" style="margin-bottom: 10px">
                         <!-- image-preview-filename input [CUT FROM HERE]-->
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename" disabled="disabled" name="fileName"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                    <span class="glyphicon glyphicon-remove"></span> ยกเลิก
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title">อัพโหลด</span>
                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                                </div>
                            </span>


                        </div>
                       
                       <!-- /input-group image-preview [TO HERE]--> 
                    </div>
                    <div class="col-sm-8">
                         <span><!-- ไฟล์ภาพ : jpg , jpeg , png , gif / --> 80px*80px (กว้าง x สูง) </span>
                    </div>

                    <div class="col-sm-12" id="cover-image" style="padding-left: 0px;">
                        <script> var dataCoverImage = <?php echo isset($coverImage) ? json_encode($coverImage) : "{}" ?> </script>
                    </div>
                    
                     
                </div>
                
                
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">ชื่อ</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>
            </div>
        </div>

        <!-- <div class="form-group">
            <label class="col-sm-1 control-label" for="excerpt">เนื้อหาย่อ</label>
            <div class="col-sm-7">
                <textarea name="excerpt" rows="3" class="form-control"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
            </div>
        </div> -->    
        <div class="form-group">
            <div class="col-sm-7 col-md-offset-2">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false"><i class="fa fa-star"></i> ประวัติการทำงาน</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-list"></i> ข้อมูลติดต่อ</a></li>
                        
                        
                    </ul>

                    <div class="tab-content" style="">
                        
                        <div class="tab-pane active" id="tab_1">
                            <div class="col-sm-12" style="margin-bottom: 15px">
                                <?php echo $this->ckeditor->editor("detail", isset($info->detail) ? $info->detail : NULL, "normal", 200); ?>
                            </div>
                            
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" >อีเมล</label>
                                <div class="col-sm-8">
                                    <input  type="text" id="" class="form-control" name="email" value="<?php echo isset($info->email) ? $info->email : NULL ?>">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-sm-2 control-label" >เบอร์โทรศัพท์</label>
                                <div class="col-sm-3">
                                    <input  type="text" id="" class="form-control" name="phoneNumber" value="<?php echo isset($info->phoneNumber) ? $info->phoneNumber : NULL ?>">
                                </div>
                                <label class="col-sm-2 control-label" >เวลาติดต่อ</label>
                                <div class="col-sm-3">
                                    <input  type="text" id="" class="form-control" name="phoneContact" value="<?php echo isset($info->phoneContact) ? $info->phoneContact : NULL ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" >ไอดีไลน์</label>
                                <div class="col-sm-8">
                                    <input  type="text" id="" class="form-control" name="idLine" value="<?php echo isset($info->idLine) ? $info->idLine : NULL ?>">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-sm-2 control-label" >Facebook</label>
                                <div class="col-sm-8">
                                    <input  type="text" id="" class="form-control" name="facebook" value="<?php echo isset($info->facebook) ? $info->facebook : NULL ?>">
                                </div>
                            </div>
                                               
                        </div>
                                            
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="form-group">
            <label class="col-sm-2 control-label" for="descript">ประวัติการทำงาน</label>
            <div class="col-sm-7">
               
            </div>
        </div>   -->

        
    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->instructorId) ? encode_id($info->instructorId) : 0 ?>">
    <?php echo form_close() ?>
</div>


<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>

    {% } %}
</script>

