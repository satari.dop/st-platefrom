<meta name="google-signin-client_id" content="14121799522-1sgjsbkij353qolfbqdpaem7l6uhbmo5.apps.googleusercontent.com">
<meta name="google-signin-scope" content="https://www.googleapis.com/auth/analytics.readonly">
<?php echo Modules::run('admin/upload/clean2') ?>

<div class="row">
  <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red-gradient"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">สมาชิกทั้งหมด</span>
                <span class="info-box-text"> <?=number_format($user);?> คน</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green-gradient"><i class="fa fa-list"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">รายการสั่งซื้อ</span>
                <span class="info-box-text"><?=number_format($course_register);?> รายการ</span>
                
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow-gradient"><i class="fa fa-book"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">คอร์สเรียน</span>
                <span class="info-box-text"> <?=number_format($course);?> คอร์ส</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

    
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua-gradient"><i class="ion ion-ios-pie-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">สถิติเข้าชมเว็บไซต์</span>
                <span class="info-box-text"><?=number_format($track);?> ครั้ง</span>
                
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<div class="row ">
    <div class="col-md-12">


        <div class="row connectedSortable">
            <div class="col-md-12">
                <div class="box box-solid bg-green-gradient">
                    <div class="box-header ">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">สถิติการลงทะเบียนเรียนประจำปี </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bg-green btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>            
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3 col-md-offset-9"><?php echo form_dropdown('year', $ddYear, current(array_keys($ddYear)), "class='form-control monthly-year '") ?></div>
                            <div class="col-md-12">
                                <!-- <div id="chart_economic"></div> -->
                                
                                <div id="monthly-course" style="min-width: 310px; height: 500px; margin: 10px auto"></div>

                            </div>
                        </div>  

                        

                    </div>
                </div>
            </div>
            
        </div>


    </div>



</div>
