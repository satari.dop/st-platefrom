<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">    

        <div class="form-group">
            <label class="col-sm-2 control-label">ชื่อ</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="excerpt">เนื้อหาย่อ</label>
            <div class="col-sm-7">
                <textarea name="excerpt" rows="3" class="form-control"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
            </div>
        </div>    

        <div class="form-group">
            <label class="col-sm-2 control-label" for="descript">เนื้อหา</label>
            <div class="col-sm-7">
                <?php echo $this->ckeditor->editor("detail", isset($info->detail) ? $info->detail : NULL, "normal", 350); ?>
            </div>
        </div>  

    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->repoId) ? encode_id($info->repoId) : 0 ?>">
    <?php echo form_close() ?>
</div>

