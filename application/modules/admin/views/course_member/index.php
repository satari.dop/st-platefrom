<input class="form-control" name="courseId" id="courseId" type="hidden" value="<?php echo $courseId;?>">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">รายการ</h3>
        <div class="box-tools pull-right">
            <?php echo $boxAction; ?>
        </div>
    </div>
    <div class="box-body filter">
        <form id="frm-filter" role="form">
            <div class="col-md-4 form-group">
                <label for="startRang">เข้าใช้งานล่าสุด</label>
                <input type="text" name="createDateRange" class="form-control" value="" />
                <input type="hidden" name="createStartDate" value="" />
                <input type="hidden" name="createEndDate" value="" />
            </div>    
            <div class="col-md-7 form-group">
                <label for="keyword">คำค้นหา</label>
                <input class="form-control" name="keyword" type="text">
            </div>           
            <div class="col-md-1 form-group">
                <button type="button" class="btn btn-primary btn-flat btn-block btn-filter"><i class="fa fa-filter"></i> ค้นหา</button>
            </div>
        </form>
    </div>
    <div class="box-body">
        <form  role="form">

            <table id="data-list" class="table table-hover dataTable" width="100%">
                <thead>
                    <tr>
                        <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                        <th>ชื่อ-นามสกุล</th>
                        <th>อีเมล์</th>
                        <th>เบอร์โทร</th>
                        <th>เข้าใช้งานล่าสุด</th>
                        <th>สร้าง</th>
                        <th></th>
                       
                    </tr>
                </thead>
            </table>
        </form>
    </div>  
    <div id="overlay-box" class="overlay">
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>         
</div>
