<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-create', 'method' => 'post')) ?>
    <div class="box-body">  
        <h4 class="block">ข้อมูลทั่วไป</h4>   
        <div class="form-group">
            <div class="col-sm-7 col-sm-offset-2">
                <label class="icheck-inline"><input type="checkbox" name="recommend" value="1" class="icheck" <?php echo isset($info->recommend) && $info->recommend == 1 ? "checked" : NULL ?>/> รายการแนะนำ</label>
            </div>
        </div>  

        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">รูปภาพหน้าปก</label>
            <div class="col-sm-7">
                <div class="row">
                    <!--  -->
                    <div class="col-sm-4" style="margin-bottom: 10px">
                         <!-- image-preview-filename input [CUT FROM HERE]-->
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename" disabled="disabled" name="fileName"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                    <span class="glyphicon glyphicon-remove"></span> ยกเลิก
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title">อัพโหลด</span>
                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                                </div>
                            </span>


                        </div>
                       
                       <!-- /input-group image-preview [TO HERE]--> 
                    </div>
                    <div class="col-sm-8">
                         <span><!-- ไฟล์ภาพ : jpg , jpeg , png , gif / --> 730px*487px (กว้าง x สูง) </span>
                    </div>

                    <div class="col-sm-12" id="cover-image" style="padding-left: 0px;">
                        <script> var dataCoverImage = <?php echo isset($coverImage) ? json_encode($coverImage) : "{}" ?> </script>
                    </div>
                    
                     
                </div>
                
                
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">ไอคอนคอร์ส</label>
            <div class="col-sm-7">
                <div class="row">
                    <!--  -->
                    <div class="col-sm-4" style="margin-bottom: 10px">
                         <!-- image-preview-filename input [CUT FROM HERE]-->
                        <div class="input-group image-preview-2">
                            <input type="text" class="form-control image-preview-filename-2" disabled="disabled" name="fileName-2"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear-2" style="display:none;">
                                    <span class="glyphicon glyphicon-remove"></span> ยกเลิก
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input-2">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title-2">อัพโหลด</span>
                                    <input type="file" accept="" name="input-file-preview-2"/> <!-- rename it -->
                                </div>
                            </span>


                        </div>
                       
                       <!-- /input-group image-preview [TO HERE]--> 
                    </div>
                    <div class="col-sm-8">
                         <span>30px*30px (กว้าง x สูง) </span>
                    </div>

                 
                     <div class="col-sm-12"  id="content-image" style="padding-left: 0px;">
                                <script> var dataContentImage = <?php echo isset($contentImage) ? json_encode($contentImage) : "{}" ?></script>
                     </div>
                    
                     
                </div>
                
                
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label"><font color=red>*</font> ชื่อคอร์ส</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" id="input-title" class="form-control" name="title" id="input-title" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="excerpt">เนื้อหาย่อ</label>
            <div class="col-sm-9">
                <textarea name="excerpt" rows="3" class="form-control"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
            </div>
        </div>    

        <div class="form-group">
            <label class="col-sm-2 control-label" for="descript">เนื้อหา</label>
            <div class="col-sm-9">

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_0" data-toggle="tab" aria-expanded="true"><i class="fa fa-list-ul"></i> รายละเอียด</a></li>
                        <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false"><i class="fa fa-star"></i> สิ่งที่ได้รับ</a></li>  
                    </ul>

                    <div class="tab-content" style="">
                        <div class="tab-pane active" id="tab_0">
                            <div class="col-sm-12" style="margin-bottom: 15px">
                                <?php echo $this->ckeditor->editor("detail", isset($info->detail) ? $info->detail : NULL, "normal", 200); ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-pane" id="tab_1">
                             <div class="col-sm-12" style="margin-bottom: 15px">
                                <?php echo $this->ckeditor->editor("receipts", isset($info->receipts) ? $info->receipts : NULL, "normal", 200); ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div> 

        <div class="form-group">
            <label class="col-sm-2 control-label"><font color=red>*</font> ผู้สอน</label>
            <div class="col-sm-4">
                
                <?php echo form_dropdown('instructorId', $instructor,  isset($info->instructorId) ? $info->instructorId : NULL ,  'class="form-control select2" required') ?>
            </div>
        </div>

         <div class="form-group">
            <label class="col-sm-2 control-label" for="recommendVideo">ลิงก์วิดีโอแนะนำคอร์ส</label>
            <div class="col-sm-7">
               <!--  <textarea name="recommendVideo" rows="3" class="form-control"><?php echo isset($info->recommendVideo) ? $info->recommendVideo : NULL ?></textarea> -->
                <input value="<?php echo isset($info->recommendVideo) ? $info->recommendVideo : NULL ?>" type="text" id="input-recommendVideo" class="form-control" name="recommendVideo" >
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="title"><font color=red>*</font> ราคา</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->price) ? number_format($info->price) : NULL ?>" type="text" class="form-control amount" name="price" required>
            </div>
        </div> 
        <div class="form-group">
            <label class="col-sm-2 control-label" for="learner">จำนวนผู้เรียน</label>
            <div class="col-sm-3">
                <input value="<?php echo isset($info->learner) ? number_format($info->learner) : NULL ?>" type="number" class="form-control amount" name="learner" >
            </div>
        </div> 

        <h4 class="block">ข้อมูล SEO</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >ชื่อเรื่อง (Title)</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->metaTitle) ? $info->metaTitle : NULL ?>" type="text" id="" class="form-control" name="metaTitle">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">คำอธิบาย (Description)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo isset($info->metaDescription) ? $info->metaDescription : NULL ?></textarea>
            </div>
        </div>   
        <div class="form-group">
            <label class="col-sm-2 control-label">คำหลัก (Keyword)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo isset($info->metaKeyword) ? $info->metaKeyword : NULL ?></textarea>
            </div>
        </div> 

        
    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->courseId) ? encode_id($info->courseId) : 0 ?>">
    <?php echo form_close() ?>
</div>
<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>



<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style="">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div id="info">
        <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
    </div>
    <div class="col-sm-4" id="action">
        <div class="btn-group">
            
           <!--  <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button> -->
        </div>
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-content-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style=" margin-bottom: 5px;">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div class="col-sm-7" id="info">
       
    </div>
    <div class="col-sm-3" id="action">
        
    </div>
    {% } %}
</script>