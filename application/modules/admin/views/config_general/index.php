<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">พื้นฐาน</h3>
    </div>
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">
        <h4 class="block">รูปแบบเว็บไซต์</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label">Logo</label>
            <div class="col-sm-2">

                <div class="library-form-wrapper">
                     <input  type="hidden" class="form-control" name="logoWeb" value="<?php echo isset($info["logoWeb"]) ? $info["logoWeb"] : NULL ?>">
                    <input type="file" name="logoWeb" accept=".gif,.jpg,.png">
                </div>
                 <div><small>กำหนดความสูง 45px</small></div>
                
            </div>
            <?php if(!empty($info["logoWeb"])){ ?>
              <div class="col-sm-2">
                <button class="btn btn-primary btn-xs" type="button" data-toggle="modal" data-target="#logoWeb"><i class="fa fa-search"></i></button> 

                <div class="modal fade" id="logoWeb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                       
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <center>
                         <img src="<?php echo base_url('uploads/config/'.$info["logoWeb"]);?>" style="max-width: 100%">
                        </center>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>   
            <?php } ?>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Favicon</label>
            <div class="col-sm-2">

                <div class="library-form-wrapper">
                     <input  type="hidden" class="form-control" name="favicon" value="<?php echo isset($info["favicon"]) ? $info["favicon"] : NULL ?>">
                    <input type="file" name="favicon" accept=".gif,.jpg,.png">
                </div>
               
            </div>
            <?php if(!empty($info["favicon"])){ ?>
              <div class="col-sm-2">
                <button class="btn btn-primary btn-xs" type="button" data-toggle="modal" data-target="#favicon"><i class="fa fa-search"></i></button> 

                <div class="modal fade" id="favicon" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                       
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <center>
                         <img src="<?php echo base_url('uploads/config/'.$info["favicon"]);?>" style="max-width: 100%">
                        </center>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>   
            <?php } ?>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" >สีหลัก</label>
            <div class="col-sm-3">
                <input value="<?php echo isset($info['color1']) ? $info['color1'] : NULL ?>" type="text" id="" class="form-control my-colorpicker1" name="color1">
            </div>
            <label class="col-sm-1 control-label" >สีรอง</label>
            <div class="col-sm-3">
                <input value="<?php echo isset($info['color2']) ? $info['color2'] : NULL ?>" type="text" id="" class="form-control my-colorpicker1" name="color2">
            </div>
        </div>
        <h4 class="block">ข้อมูลการติดต่อ</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >เบอร์โทรศัพท์</label>
            <div class="col-sm-3">
                <input value="<?php echo isset($info['phoneNumber']) ? $info['phoneNumber'] : NULL ?>" type="text" id="" class="form-control" name="phoneNumber">
            </div>
            <label class="col-sm-1 control-label" >เวลาติดต่อ</label>
            <div class="col-sm-3">
                <input value="<?php echo isset($info['phoneContact']) ? $info['phoneContact'] : NULL ?>" type="text" id="" class="form-control" name="phoneContact">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" >ไอดีไลน์</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['idLine']) ? $info['idLine'] : NULL ?>" type="text" id="" class="form-control" name="idLine">
            </div>
        </div> 
        <div class="form-group">
            <label class="col-sm-2 control-label" >Facebook</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['facebook']) ? $info['facebook'] : NULL ?>" type="text" id="" class="form-control" name="facebook">
            </div>
        </div>
        <h4 class="block">วิธีการชำระเงิน</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >รายละเอียด</label>
            <div class="col-sm-7">
                <?php echo $this->ckeditor->editor("payment", isset($info['payment']) ? $info['payment'] : NULL, "normal", 200); ?>
            </div>
        </div>

        <h4 class="block">ข้อมูล SEO</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >ชื่อเว็บไซต์ (Title)</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['siteTitle']) ? $info['siteTitle'] : NULL ?>" type="text" id="" class="form-control" name="siteTitle">
            </div>
        </div>  
        <div class="form-group">
            <label class="col-sm-2 control-label">คำอธิบายเว็บไชต์ (Description)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo isset($info['metaDescription']) ? $info['metaDescription'] : NULL ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">คำหลักเว็บไซต์ (Keyword)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo isset($info['metaKeyword']) ? $info['metaKeyword'] : NULL ?></textarea>
            </div>
        </div>          

    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <?php echo form_close() ?>
</div>