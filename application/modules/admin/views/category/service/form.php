<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
        <div class="box-tools pull-right">
        </div>
    </div>
    <div class="box-body">
        <?php echo form_open($frmAction, array('class' => 'form-horizontal', 'method' => 'post')) ?>
        <h4 class="block">ข้อมูลทั่วไป</h4>                           
        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">หมวดหมู่หลัก</label>
            <div class="col-sm-7">
                <?php echo form_dropdown('parentId', $categoryDropDown, isset($info->parentId) ? $info->parentId : NULL , 'class="form-control select2" required') ?>
            </div>
        </div>     
        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">หมวดหมู่</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->name) ? $info->name : NULL ?>" type="text" class="form-control" name="name" required>
            </div>
        </div> 
        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">อธิบายย่อ</label>
            <div class="col-sm-7">
                <textarea name="excerpt" rows="3" class="form-control"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
            </div>
        </div>    
    </div>
    <div class="box-footer">
        <div class="col-sm-7 col-sm-offset-2">
            <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="categoryType" id="input-catetory-type" value="<?php echo isset($categoryType) ? $categoryType : NULL ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->categoryId) ? decode_id($info->categoryId) : 0 ?>">
    <?php echo form_close() ?>
</div>           
</div>
