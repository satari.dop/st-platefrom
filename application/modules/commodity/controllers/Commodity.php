<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Commodity extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Repo_m");
    }

    public function index($id) {
    	$data['pageHeader']='ลำไย ปี 2561';
        $this->load->module('front');
        $data=$this->Repo_m->get_rows();
        //print"<pre>"; print_r($data); exit();
        $data['contentView'] = 'commodity/index';
        $this->front->layout($data);
    }

}
