<section class="content">
<div class="row ">
    <div class="col-md-12">
        <div class="row connectedSortable">
            <div class="col-md-8">

                <div class="nav-tabs-custom">
                        <!-- Tabs within a box -->
                        <ul class="nav nav-tabs pull-right">
                            
                          <li><a href="#sales-chart" data-toggle="tab"><i class="fa fa-bar-chart"></i> กราฟ</a></li>
                          <li class="active"><a href="#revenue-chart" data-toggle="tab"><i class="fa fa-bars"></i> ตาราง</a></li>
                          
                          <li class="pull-left header"><i class="fa fa-th"></i> ตารางการผลิตลำไยของไทย ปี 2559 - 2561 </li>
                        </ul>
                        
                        <div class="tab-content no-padding">
                          <!-- Morris chart - Sales -->
                          <div class="chart tab-pane active" id="revenue-chart" >
                              <table id="data-list" class="table table-hover dataTable" width="100%">
                            <thead>
                                <tr>
                                    <th>รายการ</th>
                                    <th width="12%">ปี 2559</th>
                                    <th width="12%">ปี 2560</th>
                                    <th width="12%">ปี 2561</th>
                                    <th width="12%">ปริมาณ</th>
                                    <th width="12%">% +/-</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1. ครัวเรือน (ครัวเรือน)</td>
                                    <td>231,569</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>2. เนื้อที่ให้ผล (ล้านไร่)</td>
                                    <td>1.05</td>
                                    <td>1.09</td>
                                    <td>1.14</td>
                                    <td>0.05</td>
                                    <td>4.59</td>
                                </tr>
                                <tr>
                                    <td>3. ผลผลิตลำไย (ล้านตัน)</td>
                                    <td>0.76</td>
                                    <td>1.02</td>
                                    <td>1.06</td>
                                    <td>0.04</td>
                                    <td>3.92</td>
                                </tr>
                                <tr>
                                    <td>4. ผลผลิตต่อไร่ (กก./ไร่)</td>
                                    <td>719</td>
                                    <td>932</td>
                                    <td>927</td>
                                    <td>-5</td>
                                    <td>-0.54</td>
                                </tr>
                                <tr>
                                    <td>5. ต้นทุนรวมต่อไร (บาท)</td>
                                    <td>6,717</td>
                                    <td>7,854</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>6. ต้นทุนรวม (บาท/กก.)</td>
                                    <td>13.20</td>
                                    <td>10.70</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>7. ราคา ณ ไร่นา <br> ลำไยสดทั้งช่อ A (บาท/กก.)</td>
                                    <td>33.24</td>
                                    <td>22.35</td>
                                    <td>29.38</td>
                                    <td>7.03</td>
                                    <td>31.45</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                     <!--  ปี 2561 พยากรณ์ไตรมาส 4 ณ เดือนธันวาคม 2560&nbsp;<br />
ราคา ปี 2561 เฉลี่ยเดือน ม.ค. - ก.พ. 2561 (เดือน ก.พ. สัปดาห์ที่ 2)<br />
&nbsp;&nbsp;<br />
&nbsp; &nbsp; &nbsp;<u>สถานการณ์</u>&nbsp;เนื้อที่ให้ผลเพิ่มขึ้น เนื่องจาก -->
                          </div>
                          <div class="chart tab-pane" id="sales-chart" >
                              <div class="col-md-12" id="container" style=" height: 400px;"></div>
                              <!-- <div class="col-md-6" id="container2" style="width: 300px; height: 300px;"></div>
                              <div class="col-md-6" id="container3" style="width: 300px; height: 300px;"></div>

                              <div class="col-md-6" id="container4" style="width: 300px; height: 300px;"></div> -->
                          </div>
                        </div>
                </div>

                <!-- <div class="box ">
                    <div class="box-header ">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">ตารางการผลิตลำไยของไทย ปี 2559 - 2561 </h3>
                        <div class="box-tools pull-right">
                           <button type="button" class="btn  btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>            
                    <div class="box-body">
                        
                    </div>
                </div> -->
                <div class="nav-tabs-custom">
                        <!-- Tabs within a box -->
                        <ul class="nav nav-tabs pull-right">
                            
                          <li><a href="#sales-chart2" data-toggle="tab"><i class="fa fa-bar-chart"></i> กราฟ</a></li>
                          <li class="active"><a href="#revenue-chart2" data-toggle="tab"><i class="fa fa-bars"></i> ตาราง</a></li>
                          
                          <li class="pull-left header"><i class="fa fa-th"></i> ตารางมูลค่าการส่งออกลำไยสดและผลิตภัณฑ์ ปี 2560 (หน่วยนับ : ล้านบาท)</li>
                        </ul>
                        
                        <div class="tab-content no-padding">
                          <!-- Morris chart - Sales -->
                          <div class="chart tab-pane active" id="revenue-chart2" >
                              <table id="data-list" class="table table-hover dataTable" width="100%">
                            <thead>
                                <tr>
                                    <th width="10" rowspan="2" style="text-align: center;">ลำดับ</th>
                                    <th colspan="8" style="text-align: center;">การส่งออก (ม.ค. - ธ.ค.)</th>
                                    
                                </tr>
                                <tr>
                                    
                                    <th style="text-align: center;" colspan="2">ลำไยสด</th>
                                    <th style="text-align: center;" colspan="2">ลำไยอบแห้ง</th>
                                    <th style="text-align: center;" colspan="2">ลำไยบรรจุภาชนะอัดลม</th>
                                    <th style="text-align: center;" colspan="2">ลำไยแช่แข็ง</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align: center;">1</td>
                                    <td width="20">เวียดนาม</td>
                                    <td width="20" style="text-align: right;">11,679.78</td>
                                    <td width="20">เวียดนาม</td>
                                    <td width="20" style="text-align: right;">7,534.79</td>
                                    <td width="20" >มาเลเซีย</td>
                                    <td width="20" style="text-align: right;">214.58</td>
                                    <td width="20">ฮ่องกง</td>
                                    <td width="20" style="text-align: right;">1.47</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">2</td>
                                     <td>จีน</td>
                                    <td style="text-align: right;">5,225.50</td>
                                    <td>จีน</td>
                                    <td style="text-align: right;">2,954.56</td>
                                    <td>เวียดนาม</td>
                                    <td style="text-align: right;">187.17</td>
                                    <td>ญี่ปุ่น</td>
                                    <td style="text-align: right;">0.79</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">3</td>
                                    <td>อินโดเนเซีย</td>
                                    <td style="text-align: right;">2,826.06</td>
                                    <td>เมียนมา</td>
                                    <td style="text-align: right;">231.72</td>
                                    <td>สิงค์โปร์</td>
                                    <td style="text-align: right;">101.07</td>
                                    <td>มาเก๊า</td>
                                    <td style="text-align: right;">0.09</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">4</td>
                                     <td>ฮ่องกง</td>
                                    <td style="text-align: right;">767.33</td>
                                    <td>ฮ่องกง</td>
                                    <td style="text-align: right;">143.25</td>
                                    <td>อินโดเนเซีย</td>
                                    <td style="text-align: right;">85.73</td>
                                    <td></td>
                                    <td style="text-align: right;"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">5</td>
                                    <td>มาเลเซีย</td>
                                    <td style="text-align: right;">195.91</td>
                                    <td>อินโดเนเซีย</td>
                                    <td style="text-align: right;">41.72</td>
                                    <td>สหรัฐอเมริกา</td>
                                    <td style="text-align: right;">77.10</td>
                                    <td></td>
                                    <td style="text-align: right;"></td>
                                </tr>
                                <tr>
                                    <th style="text-align: center;">ทั้งหมด</th>
                                    <th style="text-align: right;" colspan="2">20,970.39</th>
                                    <th style="text-align: right;" colspan="2">11,104.55</th>
                                    <th style="text-align: right;" colspan="2">753.21</th>
                                    <th style="text-align: right;" colspan="2">2.35</th>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                  
                          </div>
                          <div class="chart tab-pane" id="sales-chart2" >
                            
                              <div class="col-md-12" id="container2" style=" height: 400px;"></div>
                             
                          </div>
                        </div>
                </div>

                
                <div class="nav-tabs-custom">
                        <!-- Tabs within a box -->
                        <ul class="nav nav-tabs pull-right">
                            
                          <li><a href="#sales-chart3" data-toggle="tab"><i class="fa fa-bar-chart"></i> กราฟ</a></li>
                          <li class="active"><a href="#revenue-chart3" data-toggle="tab"><i class="fa fa-bars"></i> ตาราง</a></li>
                          
                          <li class="pull-left header"><i class="fa fa-th"></i> ตารางแสดงเนื้อที่ให้ผลและผลิตลำไย จำแนกรายภาค ปี 2561 </li>
                        </ul>
                        
                        <div class="tab-content no-padding">
                          <!-- Morris chart - Sales -->
                          <div class="chart tab-pane active" id="revenue-chart3" >
                              <table id="data-list" class="table table-hover dataTable" width="100%">
                            <thead>
                                <tr>
                                   
                                    <th style="text-align: center;" width="25%">ภาค/จังหวัด</th>
                                    <th style="text-align: center;" width="20%">เนื้อที่ให้ผล<br>(ไร่)</th>
                                    <th style="text-align: center;" width="20%">ผลผลิต<br>(ตัน)</th>
                                    <th style="text-align: center;">ผลผลิตจังหวัดที่ให้ผลได้แล้ว สูงสุด <br>5 อันดับแรก</th>
                                   
                                </tr>
                                <tr>
                                   
                                    <th>รวมทั้งประเทศ</th>
                                    <th style="text-align: right;" >1,144,952</th>
                                    <th style="text-align: right;" >1,061,322</th>
                                    <th></th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>ภาคเหนือ</td>
                                    <td style="text-align: right;">863,395</td>
                                    <td style="text-align: right;">659,266</td>
                                    <td>เชียงใหม่ ลำพูน เชียงราย พะเยา น่าน</td>
                                </tr>
                                <tr>
                                    <td>ภาคตะวันออกเฉียงเหนือ</td>
                                    <td style="text-align: right;">26,631</td>
                                    <td style="text-align: right;">16,293</td>
                                    <td>เลย หนองบัวลำภู ชัยภูมิ นครราชสีมา อุดรธานี</td>
                                </tr>
                                <tr>
                                    <td>ภาคกลาง</td>
                                    <td style="text-align: right;">254,926</td>
                                    <td style="text-align: right;">385,763</td>
                                    <td>จันทบุรี สระแก้ว ตราด ฉะเชิงเทรา สมุทรสาคร</td>
                                </tr>
                            </tbody>
                        </table>
                  
                          </div>
                          <div class="chart tab-pane" id="sales-chart3" >
                             
                              <div class="col-md-12" id="container3" style=" height: 400px;"></div>
                             
                          </div>
                        </div>
                </div>

               

            </div>

            <div class="col-md-4">
                <div class="box">
                    <div class="box-header ">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">แผนที่แสดงจังหวัดแหล่งผลิตลำไยรายภาค ปี 2561</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn  btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>            
                    <div class="box-body">
                         <div class="row">
                            <div class="col-md-12">
                                <div id="chart_map_volumn" style="height:700px"></div>
                            </div>                           
                        </div>                    
                    </div>
                </div>
            </div>   

        </div>    
    </div>
</div>  
<div class="row ">
    <div class="col-md-12">

         <div class="nav-tabs-custom">
                        <!-- Tabs within a box -->
                        <ul class="nav nav-tabs pull-right">
                            
                          <li><a href="#sales-chart4" data-toggle="tab"><i class="fa fa-bar-chart"></i> กราฟ</a></li>
                          <li class="active"><a href="#revenue-chart4" data-toggle="tab"><i class="fa fa-bars"></i> ตาราง</a></li>
                          
                          <li class="pull-left header"><i class="fa fa-th"></i> ตารางผลผลิตและราคา รายเดือน </li>
                        </ul>
                        
                        <div class="tab-content no-padding">
                          <!-- Morris chart - Sales -->
                          <div class="chart tab-pane active" id="revenue-chart4" >
                              <table id="data-list" class="table table-hover dataTable" width="100%">
                    <thead>
                       <tr>
                            <th rowspan="2" style="text-align: center;" colspan="2" >รายการ</th>
                            <th colspan="3" style="text-align: center;" >ปี 2560</th>
                            <th colspan="12" style="text-align: center;" >ปี 2561</th>
                            <th rowspan="2" style="text-align: center;" >รวม</th>
                        </tr>
                        <tr>
                            
                            <th style="text-align: center;" >ต.ค.</th>
                            <th style="text-align: center;" >พ.ย.</th>
                            <th style="text-align: center;" >ธ.ค.</th>
                            <th style="text-align: center;" >ม.ค.</th>
                            <th style="text-align: center;" >ก.พ.</th>
                            <th style="text-align: center;" >มี.ค.</th>
                            <th style="text-align: center;" >เม.ย.</th>
                            <th style="text-align: center;" >พ.ค.</th>
                            <th style="text-align: center;" >มิ.ย.</th>
                            <th style="text-align: center;" >ก.ค.</th>
                            <th style="text-align: center;" >ส.ค.</th>
                            <th style="text-align: center;" >ก.ย.</th>
                            <th style="text-align: center;" >ต.ค.</th>
                            <th style="text-align: center;" >พ.ย.</th>
                            <th style="text-align: center;" >ธ.ค.</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="150">ผลผลิตรายไตรมาส</td>
                            <td width="150">(ร้อยละ)</td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;">13.30</td>
                            <td style="text-align: center;">9.63</td>
                            <td style="text-align: center;">5.63</td>
                            <td style="text-align: center;">2.71</td>
                            <td style="text-align: center;">1.47</td>
                            <td style="text-align: center;">0.76</td>
                            <td style="text-align: center;">10.53</td>
                            <td style="text-align: center;">24.84</td>
                            <td style="text-align: center;">4.05</td>
                            <td style="text-align: center;">3.94</td>
                            <td style="text-align: center;">9.12</td>
                            <td style="text-align: center;">14.02</td>
                            <td style="text-align: right;" >100.00</td>
                        </tr>
                        <tr>
                            <td>ราคาที่เกษตรกรขายได้ ณ สัปดาห์ที่ 2 ก.พ. 61 (หน่วย:บาท/กก.)</td>
                            <td>ลำไยสดทั้งช่อพันธ์ุอีดอ เกรด A</td>
                            <td style="text-align: center;">24.50</td>
                            <td style="text-align: center;">20.82</td>
                            <td style="text-align: center;">25.61</td>
                            <td style="text-align: center;">29.29</td>
                            <td style="text-align: center;">29.50</td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: right;" ></td>
                        </tr>
                        <tr>
                            <td rowspan="4">ราคาส่งออก (หน่วย:บาท/กก.)</td>
                            <td>ลำไยสด</td>
                            <td style="text-align: center;">30.16</td>
                            <td style="text-align: center;">28.27</td>
                            <td style="text-align: center;">25.52</td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: right;" ></td>
                        </tr>
                        <tr>
                            <td>ลำไยอบแห้ง</td>
                            <td style="text-align: center;">51.19</td>
                            <td style="text-align: center;">49.59</td>
                            <td style="text-align: center;">52.68</td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: right;" ></td>
                        </tr>
                        <tr>
                            <td>ลำไยบรรจุภาชนะอัดลม</td>
                            <td style="text-align: center;">69.65</td>
                            <td style="text-align: center;">79.34</td>
                            <td style="text-align: center;">68.78</td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: right;" ></td>
                        </tr>
                        <tr>
                            <td>ลำไยแช่แข็ง</td>
                            <td style="text-align: center;">-</td>
                            <td style="text-align: center;">-</td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: right;" ></td>
                        </tr>
                        
                    </tbody>
                </table>
                  
                          </div>
                          <div class="chart tab-pane" id="sales-chart4" >
                             
                              <div class="col-md-12" id="container4" style=" height: 400px;"></div>
                             
                          </div>
                        </div>
                </div>

        <!-- <div class="box ">
            <div class="box-header ">
                <i class="fa fa-th"></i>
                <h3 class="box-title">ตารางผลผลิตและราคา รายเดือน</h3>
                <div class="box-tools pull-right">
                   <button type="button" class="btn  btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>            
            <div class="box-body">
                
              
            </div>
        </div>   -->
    </div>
</div>   

<div class="row ">
    <div class="col-md-12">
        <div class="box ">
            <div class="box-header ">
                <i class="fa fa-th"></i>
                <h3 class="box-title">มาตรการภาครัฐ</h3>
                <div class="box-tools pull-right">
                   <button type="button" class="btn  btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>            
            <div class="box-body">
                1. กระทรวงเกษตรและสหกรณ์มีนโยบายส่งเสริม
การปลูกในเขตพื้นที่เหมาะสม (Zoning) และตามแผนที่
เกษตร (Agri - Map) ในลักษณะระบบส่งเสริมการเกษตร
แบบแปลงใหญ่ตามระบบ GAP เพื่อให้สอดคล้องกับ
ความต้องการของตลาด<br>
                2. ดำเนินการตามยุทธศาสตร์พัฒนาผลไม้ไทยปี2558-
2562 ใน 5 ยุทธศาสตร์ ได้แก่ การบริหารจัดการด้าน
การผลิตด้านการตลาดการวิจัยและพัฒนา พัฒนาองค์กร
และเกษตรกร และการพัฒนาระบบฐานข้อมูลสารสนเทศ
พร้อมกับการขับเคลื่อนยุทธศาสตร์พัฒนาผลไม้เป็น
รายภาค โดยเน้นเรื่องคุณภาพและความปลอดภัยเป็น
สิ่งสำคัญ<br>
                3. บริหารจัดการอุปทานให้สอดคล้องกับอุปสงค์
ในระดับจังหวัด<br>
                4. ลดต้นทุนการผลิต โดยลดการใช้ปัจจัยการผลิต
เพิ่มผลผลิตต่อไร่ เพิ่มช่องทางบริหารจัดการ วางแผน
รวมทั้งเพิ่มช่องทางการตลาด<br>
                
            </div>
        </div>  
    </div>
</div>  
</section>