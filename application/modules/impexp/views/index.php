<section class="content">
<div class="row ">
    <div class="col-md-12">
           <div class="row connectedSortable">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header ">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">ภาพรวมปริมาณนำเข้า-ส่งออก</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>            
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="chart_price_index"></div>
                            </div>
                        </div>                 
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header ">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">ภาพรวมมูลค่านำเข้า-ส่งออก</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>            
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="chart_product_index"></div>
                            </div>
                        </div>                 
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>   
</section>