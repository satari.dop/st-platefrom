<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <base href="<?php echo site_url(); ?>">
        
        <title><?php echo $pageTitle; ?></title>
        <meta name="description" content="<?php echo $description; ?>" />
        <meta name="keywords" content="<?php echo $keywords; ?>" />
        <meta name="author" content="" />

        <?php echo Modules::run('social/share'); ?>

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

 

  <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/website/") ?>css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/website/") ?>css/jquery.animatedheadline.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/website/") ?>css/materialize.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/website/") ?>css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/website/") ?>css/skins/orange.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/website/") ?>css/custom.css" />
  <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">

    <!-- Template JS Files -->
    <script src="<?php echo base_url("assets/website/") ?>js/modernizr.custom.js"></script>
<style type="text/css"></style>
    </head>
  
<script src="//platform-api.sharethis.com/js/sharethis.js#property=5b782303795066001124e12b&product=inline-share-buttons"></script>

<!-- 
<link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>images-grid/src/images-grid.css"> -->


  <link rel='stylesheet' href='<?php echo base_url("assets/website/") ?>unitegallery-master/package/unitegallery/css/unite-gallery.css' type='text/css' />
  

  <!-- Ion Slider -->
  <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>ionslider/ion.rangeSlider.css">
  <!-- ion slider Nice -->
  <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>ionslider/ion.rangeSlider.skinNice.css">
  <!-- bootstrap slider -->
  <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>bootstrap-slider/slider.css">

<link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>nice-select/css/nice-select.css">


    <body class="blog">
       <?php $this->load->view('search'); ?>

       <nav class="navbar navbar-default navbar-static-top ">
        <div class="container">
           <div class="row">
             <div class="content col s12 m8 l8 xl8 font_20">
              <a href="<?php echo site_url();?>">
                <?php echo $header;?>
                </a>
             </div>
           </div>
        </div>
      </nav>
    <!-- Preloader Start -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- Preloader Ends -->

    <!-- Wrapper Starts -->
    <div class="wrapper">

    
     <?php $this->load->view($contentHeader); ?>
    <!-- Divider Starts -->
    <div class="divider center-align">
      <span class="outer-line"></span>
     <?php echo $header;?>
      <span class="outer-line"></span>
    </div>
    <!-- Divider Ends -->


    <div class="container">
      <div class="row">
        <div class="content col s12 m8 l8 xl8">

          <?php $this->load->view($contentView); ?>

          
        </div>

        <?php $this->load->view('sidebar'); ?>
         <div  class="clearfix"></div>
      </div>
    </div>

    <?php $this->load->view('footer'); ?>
    </div>
    <!-- Wrapper Ends -->        


<!-- Template JS Files -->
  <script src="<?php echo base_url("assets/website/") ?>js/jquery-2.2.4.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js"></script>
  <script src="<?php echo base_url("assets/website/") ?>js/jquery.animatedheadline.js"></script>
  <script src="<?php echo base_url("assets/website/") ?>js/boxlayout.js"></script>
  <script src="<?php echo base_url("assets/website/") ?>js/materialize.min.js"></script>
  <script src="<?php echo base_url("assets/website/") ?>js/jquery.hoverdir.js"></script>
  <script src="<?php echo base_url("assets/website/") ?>js/custom.js"></script>
  <script src="<?php echo base_url("assets/website/") ?>js/footer.js"></script>
<!--<script src="<?php echo base_url("assets/website/") ?>images-grid/src/images-grid.js"></script>
 
<script type='text/javascript' src='<?php echo base_url("assets/website/") ?>unitegallery-master/package/unitegallery/js/jquery-11.0.min.js'></script>  -->
  <script type='text/javascript' src='<?php echo base_url("assets/website/") ?>unitegallery-master/package/unitegallery/js/unitegallery.min.js'></script>  
  <script type='text/javascript' src='<?php echo base_url("assets/website/") ?>unitegallery-master/package/unitegallery/themes/tiles/ug-theme-tiles.js'></script>


  <!-- Ion Slider -->
<script src="<?php echo base_url("assets/website/") ?>ionslider/ion.rangeSlider.min.js"></script>
<!-- Bootstrap slider -->
<script src="<?php echo base_url("assets/website/") ?>bootstrap-slider/bootstrap-slider.js"></script>


  <script src="<?php echo base_url("assets/website/") ?>nice-select/js/jquery.nice-select.js"></script>
  <script>
       // new gnMenu( document.getElementById( 'gn-menu' ) );
    </script>



        <?php echo $pageScript; ?>
        <script>
           function get_cookie(name) {

                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ')
                        c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0)
                        return c.substring(nameEQ.length, c.length);
                }
                return null;
            }
            var csrfToken = get_cookie('csrfCookie');
            var siteUrl = "<?php echo site_url(); ?>";
            var baseUrl = "<?php echo base_url(); ?>";
            var controller = "<?php echo $this->router->class ?>";
            var method = "<?php echo $this->router->method ?>";
            $(document).ready(function () {
                <?php if ($this->session->toastr) : ?>
                    setTimeout(function () {
                        toastr.<?php echo $this->session->toastr['type']; ?>('<?php echo $this->session->toastr['lineTwo']; ?>', '<?php echo $this->session->toastr['lineOne']; ?>');
                    }, 500);
                    <?php $this->session->unset_userdata('toastr'); ?>
                <?php endif; ?>
                //alert(1);
               $('.slider').slider();

                var min_price = "<?php echo $min_price; ?>";
                var max_price = "<?php echo $max_price; ?>";

                $("#range").ionRangeSlider({
                  min: min_price,
                  max: max_price,
                  from: min_price,
                  to: max_price,
                  type: 'double',
                  step: 500,
                  prefix: "฿ ",
                  prettify: false,
                  hasGrid: true
                });

                
                
                 
                  $(".type-select").niceSelect();
                  $(".location-select").niceSelect();

                  $(".type-select").change(function(){
                    //$(".location").empty();
                    
                    var type = $(".type-select").val();
                    
                    $.post(siteUrl+'front/getLocation',{csrfToken: csrfToken , type:type },function(data){
                    //console.log(data);
                         if(data){  
                            
                             if($("#location").val()==""){
                               $(".location-select").html(data); 
                             }
                               $('select.location-select').niceSelect('update');
                             
                          }
                    });

                });

                $(".location-select").change(function(){
                    //$(".location").empty();
                    
                    var location = $(".location-select").val();
                    
                    $.post(siteUrl+'front/getType',{csrfToken: csrfToken , location:location },function(data){
                    //console.log(data);
                         if(data){  
                            
                             if($("#type").val()==""){
                               $(".type-select").html(data); 
                             }
                               $('select.type-select').niceSelect('update');
                             
                          }
                    });

                });   
                
            });   

            
            //console.log(images_g);
               
        </script>


         
    </body>
</html>
