<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				
				<span class="copyright">© 2019 <?=$siteName?>. All rights reserved.</span>
			</div>
		</div>
	</div><!-- container -->
</footer>

		<!-- Modal -->
		<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<img src="<?php echo base_url("assets/website/") ?>images/logo.jpg" class="ls-bg" alt="" style="width: 45px;"/>
						<!--  <h5 class="modal-title" id="exampleModalLabel">เข้าสู่ระบบ</h5> -->
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="login-box-body">
							<div class="loginmodal-container">
								<h1>เข้าสู่ระบบ</h1><br>
								<?php
								$form=array('accept-charset'=>'utf-8','class'=>'form-horizontal','id'=>'frm-login','name'=>'frm-login'); 
								echo form_open('/user/login/check', $form);
								echo form_hidden('base_url',base_url());
								echo form_hidden('site_url',site_url());

								?>
								<input type="text" name="username" id="username_login" placeholder="Username">
								<input type="password" name="password" id="password_login" placeholder="Password">
								<div id="form-success-div_login" class="text-success"></div> 
								<div id="form-error-div_login" class="text-danger"></div>
								<button type="submit" name="login" class="login btn-login" >เข้าสู่ระบบ</button> 
								<a href="<?=$authUrl?>" class="login btn-facebook"><i class="fa fa-facebook"></i> เข้าสู่ระบบด้วย Facebook</a>

								<a href="<?php echo site_url("user/register/") ?>" class="login btn-register-l">สมัครสมาชิก</a>
								<?php echo form_close(); ?>

								<div class="login-help">
									<!-- <a href="<?php echo site_url("user/register/") ?>">สมัครสมาชิก</a> -->  <!-- - <a href="#">Forgot Password</a>  -->
								</div>
							</div>



						</div>

					</div>
					<div class="modal-footer">
<!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary">Save changes</button> -->
</div>
</div>
</div>
</div>

<!-- Load Facebook SDK for JavaScript -->
<!-- <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

Your customer chat code
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="122499241186246">
</div> -->

<!-- cahat -->

<div class="floating-chat enter">
	<a href="https://m.me/<?=$facebook?>" target="_blank" class="chat_a">
		<i class="fa fa-comments chat" aria-hidden="true"></i>
		<div class="chat">
			<div class="header">

			</div>
		</div>
	</a>
</div>
