<header>

            <div class="top-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-xs-2">
                            <div class="logo">
                                <a href="<?php echo site_url('') ?>">
                                <?php echo $logoWeb; ?> 
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-10 col-xs-10">

                            <div class="right-area login-t"> 
                                 <?php if ( !$isLogin) : ?>
                                <a class="b-regis" href="<?php echo site_url("user/register/") ?>">
                                   <img src="<?php echo base_url("assets/website/") ?>images/icon/icon-member-w.svg" style="width: 27px; height: 27px;" />
                                    <span>สมัครสมาชิก |</span>
                                </a>
                                <?php else : ?>

                                 <a class="b-name" href="<?php echo site_url("user/profile/") ?>">
                                    <!-- <img src="<?php echo base_url("assets/website/") ?>images/icon/icon-member-w.svg"  /> -->

                              <img alt="K SME" src="<?php echo $this->session->member['image'];?>" style="border-radius: 50%;">
                           
                                    <span><?php echo $this->session->member['name'] ?></span> 
                                </a>
                                <span>|</span>
                                 <?php endif; ?>
                                 <?php if ( !$isLogin) : ?>
                                <a  class="b-login" href="#" data-toggle="modal" data-target="#login">
                                    <img src="<?php echo base_url("assets/website/") ?>images/icon/icon-login-w.svg"  style="width: 27px; height: 27px;"/>
                                    <span>เข้าสู่ระบบ</span>
                                </a>
                                <?php else : ?>
                                    <a  class="b-logout" href="<?php echo site_url("logout") ?>" >
                                            <img src="<?php echo base_url("assets/website/") ?>images/icon/icon-login-w.svg"  style="width: 27px; height: 27px;"/>
                                    <span>ออกจากระบบ</span>
                                    </a>
                                    <a  class="b-history" href="<?php echo site_url("course/course_history") ?>">
                                       <img src="<?php echo base_url("assets/website/") ?>images/icon/icon-car-w.svg"  style="width: 27px; height: 27px;"/>
                                        <span>รายการสั่งซื้อ</span> 
                                    </a>
                                <?php endif; ?>
                                



                            </div>
                            <!-- right-area -->
                        </div>
                    </div>
                </div>
            </div><!-- top-menu -->



            <div class="bottom-area">

                <div class="menu-nav-icon" data-nav-menu="#main-menu"><i class="ion-navicon"></i></div>

                <ul class="main-menu visible-on-click" id="main-menu">
                    <li class="<?php if(!empty($home_act)){ echo $home_act; } ?> "><a href="<?php echo site_url() ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-compass.svg" alt="Logo Image" >อ.กอร่า</a></li>
                    <li class="<?php if(!empty($article_act)){ echo $article_act; } ?>"><a href="<?php echo site_url('article') ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-text.svg" alt="Logo Image" > บทความกราฟฟิก</a></li>
                    <li class="drop-down <?php if(!empty($course_act)){ echo $course_act; } ?>"><a href="javascript:void(0)"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-pen.svg" alt="Logo Image" > คอร์สเรียนออนไลน์<i class="ion-ios-arrow-down">

                    </i></a>
                    <ul class="drop-down-menu">
                        <li class="drop-down"><a href="#!">CATEGORIES<i class="ion-ios-arrow-right"></i></a>
                            <ul class="drop-down-menu drop-down-inner">
                                <li><a href="#">FEATURED</a></li>
                                <li><a href="#">ABOUT</a></li>
                                <li><a href="#">CATEGORIES</a></li>
                            </ul>
                        </li>
                    <?php echo Modules::run('course/top_menu') ?>
                    </ul>
                </li>
                
                <li class="<?php if(!empty($activity_act)){ echo $activity_act; } ?> "><a href="<?php echo site_url('activity') ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-pencil.svg" alt="Logo Image" > สอนสด</a></li>

                <?php if($isLogin){ ?>
                <li ><a target="_blank" href="https://www.facebook.com/groups/2229423787381163/" onclick="updateCommunity()"><img src="<?php echo base_url("assets/website/") ?>images/icon/commu.svg" alt="Logo Image" > Community</a></li> 
                <?php } ?>
                <li  class="<?php if(!empty($contact_act)){ echo $contact_act; } ?> "><a href="<?php echo site_url('contact') ?>"><img src="<?php echo base_url("assets/website/") ?>images/icon/icon-contactus.svg" alt="Logo Image" > ติดต่อเรา</a></li>
            </ul><!-- main-menu -->

        </div><!-- conatiner -->
    </header>