<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <base href="<?php echo site_url(); ?>">
    <title><?php echo $pageTitle; ?></title>
    <meta name="description" content="<?php echo $metaDescription; ?>" />
    <meta name="keywords" content="<?php echo $metaKeyword; ?>" />
    <meta name="author" content="" />
     <link rel="icon" href="<?php echo $favicon ?>" type="image/x-icon"> 

    <?php echo Modules::run('social/share'); ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link href="<?php echo base_url("assets/website/") ?>fonts/font.css" rel="stylesheet">

    <!-- Stylesheets -->

    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>plugins/font-awesome/css/font-awesome.min.css?v=2.1.5">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>plugins/Ionicons/css/ionicons.min.css?v=2.1.5">
    

    <link href="<?php echo base_url("assets/website/") ?>common-css/bootstrap.css?v=2.1.5" rel="stylesheet">


    <link href="<?php echo base_url("assets/website/") ?>common-css/layerslider.css?v=2.1.5" rel="stylesheet">

    <link href="<?php echo base_url("assets/website/") ?>css/styles.css?v=2.1.5" rel="stylesheet">

    
    <link href="<?php echo base_url("assets/website/") ?>css/custom.css?v=2.1.5" rel="stylesheet">
    <link href="<?php echo base_url("assets/website/") ?>css/responsive.css?v=2.1.5" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>vnotify/dist/vanilla-notify.css" />
    <!-- add carousel -->
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>OwlCarousel2/docs/assets/owlcarousel/assets/owl.carousel.min.css?v=2.1.5">
    
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/owl-modify.css" type="text/css?v=2.1.5" />

    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/jquery.fancybox.css?v=2.1.5" />
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />

    <link href='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/fullcalendar.min.css?v=2.1.5' rel='stylesheet' />
    <link href='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/fullcalendar.print.min.css?v=2.1.5' rel='stylesheet' media='print' />

    <link href="<?php echo base_url() ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css"/>      
     <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/timepicker/bootstrap-timepicker.min.css">


    <link rel='stylesheet' href='<?php echo base_url("assets/website/") ?>unitegallery-master/package/unitegallery/css/unite-gallery.css?v=2.1.5' type='text/css' />

    <link rel='stylesheet' href='<?php echo base_url("assets/website/") ?>sweetalert2/package/dist/sweetalert2.min.css?v=2.1.5' type='text/css' />

    <?php //if (in_array($this->router->class, array('user'))){ ?>
  <!--   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css"> -->

    <?php //} ?>

    <!-- <link rel="stylesheet" href="https://cdn.plyr.io/3.4.4/plyr.css"> -->
    <link rel='stylesheet' href='<?php echo base_url("assets/website/") ?>plyr-master/dist/plyr.css' type='text/css' />

    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=59816b9dabd6b200117460b3&product=inline-share-buttons"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127405874-1"></script>
    <!-- Zun Noti -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/0.0.11/push.min.js"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-127405874-1');
  </script>

<style type="text/css">
.red-tooltip + .tooltip > .tooltip-inner {background-color: #f00;}
.red-tooltip + .tooltip > .tooltip-arrow { border-bottom-color:#f00; }

header .top-menu{ 
   background-color: <?php echo $color1;?>;
 }

 header .bottom-area {
   
    background-color: <?php echo $color2;?>;
   
}
footer {
    background:  <?php echo $color2;?>;
   
}
.single-post {
    background-color: <?php echo $color2;?>;
    
}
</style>

</head>
<body class="layout-boxed">
    <div class="wrapper">
        <?php $this->load->view('topmenu'); ?>


        <?php $this->load->view($contentView); ?>

        <?php $this->load->view('footer'); ?>
        <input type="hidden" name="after_login_url" id="after_login_url" value="<?php echo site_url('');?>">
    </div>        
    


    <script src="<?php echo base_url("assets/website/") ?>common-js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url("assets/website/") ?>common-js/jquery.countdown.min.js"></script>
    <script src="<?php echo base_url("assets/website/") ?>common-js/tether.min.js"></script>

    <script src="<?php echo base_url("assets/website/") ?>common-js/bootstrap.js"></script>

    <script src="<?php echo base_url("assets/website/") ?>common-js/layerslider.js"></script>

    <script src="<?php echo base_url("assets/website/") ?>common-js/scripts.js"></script>
    
    <script src="<?php echo base_url("assets/website/") ?>vnotify/vanilla-notify.js"></script>
    <!--add owl-carousel-->
    <script  src="<?php echo base_url("assets/website/") ?>OwlCarousel2/docs/assets/owlcarousel/owl.carousel.min.js" type="text/javascript" ></script>
    <script  src="<?php echo base_url("assets/website/") ?>common-js/owl-carousel-function2.js" type="text/javascript" ></script>

     <script src="<?php echo base_url() ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

      <!-- bootstrap time picker -->
<script src="<?php echo base_url();?>assets/bower_components/timepicker/bootstrap-timepicker.min.js"></script>


    <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/jquery.fancybox.js?v=2.1.5" ></script>
    <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    <?php if (in_array($this->router->class, array('course'))) : ?>
      <script src="<?php echo base_url("assets/website/") ?>vimeo-player/dist/player.js"></script>
     
<!-- <script src="https://cdn.plyr.io/3.4.4/plyr.js"></script> -->
<script src="<?php echo base_url("assets/website/") ?>plyr-master/dist/plyr.js"></script>
<script src="https://player.vimeo.com/api/player.js"></script>

  <?php endif; ?>
  
<?php //if (in_array($this->router->class, array('user'))){ ?>
<!--    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
   <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
   <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
   <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script> -->
<?php //} ?>
  <?php if (in_array($this->router->class, array('activity'))) : ?>
    <script src='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/lib/moment.min.js'></script>
    <script src='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/lib/jquery.min.js'></script>
    <script src='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/fullcalendar.min.js'></script>
    <script src='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/locale/th.js'></script>


    <script type='text/javascript' src='<?php echo base_url("assets/website/") ?>unitegallery-master/package/unitegallery/js/unitegallery.min.js'></script>  
    <script type='text/javascript' src='<?php echo base_url("assets/website/") ?>unitegallery-master/package/unitegallery/themes/tiles/ug-theme-tiles.js'></script>
<?php endif; ?>


<script type='text/javascript' src='<?php echo base_url("assets/website/") ?>sweetalert2/package/dist/sweetalert2.all.min.js'></script>

<?php //if (in_array($this->router->class, array('user'))) : ?>
<script>
  var recaptcha_sitekey = "<?php echo $recaptcha_sitekey ?>";
  var recaptcha_secretkey = "<?php echo $recaptcha_secretkey ?>";
</script>
<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit' async defer></script>
<?php //endif; ?>

<?php echo $pageScript; ?>
<script>
           <?php if($isCommunity==0){ ?>

            vNotify.warning({text:'สิทธิพิเศษสำหรับสมาชิกรออยู่เพียบ', title:'กดเข้าร่วม Community'});

           <?php } ?>

            function get_cookie(name) {

                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ')
                        c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0)
                        return c.substring(nameEQ.length, c.length);
                }
                return null;
            }
           

            
            var csrfToken = get_cookie('csrfCookie');
            var siteUrl = "<?php echo site_url(); ?>";
            var baseUrl = "<?php echo base_url(); ?>";
            var controller = "<?php echo $this->router->class ?>";
            var method = "<?php echo $this->router->method ?>";


            $(document).ready(function () {
                <?php if ($this->session->toastr) : ?>
                    setTimeout(function () {
                        toastr.<?php echo $this->session->toastr['type']; ?>('<?php echo $this->session->toastr['lineTwo']; ?>', '<?php echo $this->session->toastr['lineOne']; ?>');
                    }, 500);
                    <?php $this->session->unset_userdata('toastr'); ?>
                <?php endif; ?>

                // checkloginuser();

            }); 

            function updateCommunity() {
                // alert(1);
                $.post(siteUrl+'user/login/updateCommunity',{csrfToken: csrfToken},function(data){
                        
                });
                
               
            }

            function setLoginAfterurl(url){
                $("#after_login_url").val(url);
            }

            

            jQuery(document).ready(function() {
                jQuery('.tabs .tab-links a').on('click', function(e) {
                    var currentAttrValue = jQuery(this).attr('href');

                    // Show/Hide Tabs
                    jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

                    // Change/remove current tab to active
                    jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

                    e.preventDefault();
                });
            }); 

            // $(function(){
            //     // กำหนดให้สคริปเช็คสถานะการใช้งานอยู่ของสมาชิกและอัพเดทเวลา ให้ทำงานทุกๆ 5 วินาที
            //     setInterval(function(){
            //        checkloginuser();
            //     },1000 * 1200);  // 1000 เท่ากับ 1 วินาที
            // });  

            //  function logoutFunction(){
            //     $.post(siteUrl+'/logout/logoutFunction',{csrfToken: csrfToken},function(data){
            //         ////
            //     });
            // }


            // function checkloginuser(){
            //     $.post(siteUrl+'/user/login/checkloginuser',{csrfToken: csrfToken},function(data){
            //        // console.log(data);
            //             if(data==0){ 
            //                 logoutFunction();
            //             }
            //     });
            // }

            // $(function(){
            //     $(window).bind("beforeunload",function(event){
            //         var msg="ยืนยันการปิดหน้านี้?";
            //         $(window).bind("unload",function(event){
            //             event.stopImmediatePropagation();
            //             $.ajax({
            //                 type: 'get',
            //                 async: false,
            //                 url: siteUrl+'/logout',
            //                 success:function(){ 

            //                 }
            //             });
            //         });
            //         return msg;
            //     });
            //     $("a").click(function(){ // กรณีคลิกลิ้งค์ ไม่ต้องแสดง การแจ้งเตือน
            //         $(window).unbind("beforeunload");
            //     });
            // });

            function windowPopup(url, width, height) {
                // Calculate the position of the popup so
                // it’s centered on the screen.
                var left = (screen.width / 2) - (width / 2),
                    top = (screen.height / 2) - (height / 2);

                window.open(
                  url,
                  "",
                  "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left
                );
              }

              //jQuery
              $(".js-social-share").on("click", function(e) {
                e.preventDefault();

                windowPopup($(this).attr("href"), 500, 300);
              });

              // Vanilla JavaScript
              var jsSocialShares = document.querySelectorAll(".js-social-share");
              if (jsSocialShares) {
                [].forEach.call(jsSocialShares, function(anchor) {
                  anchor.addEventListener("click", function(e) {
                    e.preventDefault();

                    windowPopup(this.href, 500, 300);
                  });
                });
              }

              $(document).ready(function(){
                    $("span").tooltip();
                     $("button").tooltip();
                });
        
        </script>
        

        <script src="<?php echo base_url(); ?>assets/scripts/front/login.js"></script>
        <script src="<?php echo base_url(); ?>assets/scripts/front/totop.js"></script>


    </body>
    </html>
