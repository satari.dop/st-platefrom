<!-- Portfolio Starts -->
<section id="bl-work-section">
  <!-- Portfolio Title Starts -->
  <div class="bl-box valign-wrapper">
    <div class="page-title center-align">
      <span class="title-bg">HOUSE</span>
      <h2 class="center-align">
        <span data-hover="บ้าน">บ้าน </span>
        <span data-hover="มือสอง">มือสอง</span>
      </h2>
    </div>
  </div>
  <!-- Portfolio Title Ends -->
  <!-- Portfolio Expanded Starts -->
  <div class="bl-content">
    <!-- Main Heading Starts -->
    <div class="container page-title center-align">
      <h2 class="center-align">
        <span data-hover="บ้าน">บ้าน </span>
        <span data-hover="มือสอง">มือสอง</span>
      </h2>
      <span class="title-bg">HOUSE</span>
    </div>
    <!-- Main Heading Ends -->
    <div class="container">
      <!-- Divider Starts -->
      <div class="divider center-align">
        <span class="outer-line"></span>
       <?php echo $header;?>
        <span class="outer-line"></span>
      </div>
      <!-- Divider Ends -->
      <div class="row center-align da-thumbs" >

        <?php if(!empty($info)){ foreach ($info as $key => $rs) { ?>
         
        <!-- Project Starts -->
        <div class="col s12 m4 l4 xl4" style="padding-bottom: 20px;">
          <!-- Picture Starts -->
           <div class="col s12 m12 l12 xl12">

            <div class="container_blog">
              <a href="<?php echo site_url("house/detail/{$rs['linkId']}");?>">
              <img src="<?php echo $rs['image'] ?>" alt="Avatar" class="responsive-img image_blog">
              <div class="overlay_blog">
                <div class="text_blog"><?php echo $rs['title'] ?></div>
              </div>
            </a>
            </div>
            
           </div>
          <!-- Picture Ends -->
          <div class="col s12 m12 l12 xl12 personal-info " style="padding-top: 15px;">
            <h6 class="second-font left-align"><a href="<?php echo site_url("house/detail/{$rs['linkId']}");?>" style="color: #fa5b0f;"><?php echo $rs['title'] ?></a></h6>
            <p class="left-align"><?php echo iconv_substr($rs['excerpt'],0,120, "UTF-8").'...'; ?><span class="read-more"><a href="<?php echo site_url("house/detail/{$rs['linkId']}");?>" >อ่านต่อ</a></span></p>
             <p class="left-align">สถานที่ : <?php echo $rs['location'] ?></p>
             <p><h6 class="left-align"><b>ราคา : <?php echo price_th($rs['price']) ?></b></h6></p>
          
          <div style="margin-top: 5px;"> 
          <a href="<?php echo site_url("house/detail/{$rs['linkId']}");?>" class="col s12 m12 l12 xl12 waves-effect waves-light btn font-weight-500">
           รายละเอียด</i>
         </a>
        </div>
       </div>
       </div>
      <?php if(($key+1)%3==0){ ?>
      <div class="clearfix"></div>
      <?php }  ?>
       <!-- Project Ends -->
     <?php } } ?>
     

       
     <!-- Project Ends -->
     
</div>
</div>
</div>
<!-- Portfolio Expanded Ends -->
<img alt="close" src="<?php echo base_url("assets/website/") ?>images/close-button.png" class="bl-icon-close" />
</section>
<!-- Portfolio Section Ends -->



