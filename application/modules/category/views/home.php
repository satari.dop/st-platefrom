    <div class="owl-carousel owl-theme slidlink" id="slidlink">
        <?php foreach ( $info as $key=>$rs) :?>
        <?php $i = ($key+1) % 3 ?>
        <?php if ($i == 1) : ?>
        <div class="item">
        <?php endif; ?>
            <div class="col-sm-12 link">
                <a href="<?php echo site_url("category/id/{$rs['categoryID']}"); ?>">
                    <img src="<?php echo $rs['image'] ?>" class="img-responsive">
                    <div class="caption">
                        <h1 class="color4 bold"><?php echo $rs['categoryName'] ?></h1>
                        <h3 class="color5"><?php echo $rs['categoryExcerpt'] ?></h3>
                    </div>
                </a>
            </div>
        <?php if ($i == 0 || $key == $numRow ) : ?>
        </div>
        <?php endif; ?>
        <?php endforeach; ?>      
    </div> 
