<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Social_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_list()
    {
        $query = $this->db
                        ->where('socialActive', 1)
                        ->order_by('socialOrder', 'asc')
                        ->get('social')
                        ->result_array();
        return $query;
    }
    

        
}