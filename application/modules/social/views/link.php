<div class="socail center-block">
  <?php foreach ($info as $rs) :?>
      <span class="btnsocail">
          <a title="<?php echo $rs['socialTitle'] ?>" href="<?php echo $rs['socialLink'] ?>">
              <img src="<?php echo $rs['image'] ?>" alt="<?php echo $rs['socialTitle'] ?>" >
          </a>
      </span>
<?php endforeach; ?>
</div>