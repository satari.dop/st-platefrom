<!-- <div class="main-slider">
    <div id="slider">
        <?php foreach ($info as $key => $rs) :?> -->
        <!--<div class="ls-slide" data-ls="bgsize:cover; bgposition:50% 50%; duration:4000; transition2d:104; kenburnsscale:1.00;">
          <img src="<?php echo $rs->image ?>" class="ls-bg responsive" alt="" />

           <div class="slider-content ls-l" style="top:60%; left:30%;" data-ls="offsetyin:100%; offsetxout:-50%; durationin:800; delayin:100; durationout:400; parallaxlevel:0;">
            <a class="btn" href="#">TRAVEL</a>
            <h3 class="title"><b>Travel, Love, Live</b></h3>
            <h6>29 October, 2017</h6>
          </div> 

        </div>-->
        
        <!-- ls-slide -->
         <!--  <?php endforeach; ?> 
        

      </div>
    </div> --><!-- main-slider -->


<div class="main-slider" id="slider-c">
  <div id="owl-example" class="owl-carousel owl-theme">
    <?php foreach ($info as $rs) :?>
    
    <div class="item ">
       <?php if ($rs->link){ ?>
          <a href="<?php echo $rs->link ?>" >
           <div class="banner-pc">
              <img src="<?php echo $rs->image['coverImage'] ?>" class="img-responsive">
           </div>
           <div class="banner-mobile">
                <img src="<?php echo $rs->image['contentImage'] ?>" class="img-responsive">
          </div>
          </a>
        <?php }else{ ?>
           <div class="banner-pc">
              <img src="<?php echo $rs->image['coverImage'] ?>" class="img-responsive">
           </div>
           <div class="banner-mobile">
                <img src="<?php echo $rs->image['contentImage'] ?>" class="img-responsive">
          </div>
        <?php } ?>
    </div>
    
   
    <?php endforeach; ?>
  </div> 
</div>