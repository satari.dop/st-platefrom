<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MX_Controller {
    
    private $_grpContent = 'banner';

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('banner_m');
    }
    
    public function hero($page=0)
    {
        $input['type']='hero';
        $input['active']='1';
        $input['recycle']='0';
        $input['page']=$page;

        $info = $this->banner_m->get_list($input)->result();
       
        
        if ( !empty($info) ) {
            foreach ( $info as $key=>&$rs ) {
                $input_u['grpContent'] = $this->_grpContent;
                $input_u['contentId'] = $rs->bannerId;
                // $file_ = $this->banner_m->get_uplode($input_u)->row();
                // if (!empty($file_) && is_file($file_->path.$file_->filename)) {
                //     $rs->image = base_url($file_->path.$file_->filename);
                
                // }

                $file_ = $this->course_m->get_uplode($input_u)->result();
                   
                if (!empty($file_)) {
                    foreach ($file_ as $key => $value) {
                        if($value->grpType=='coverImage'){
                            $rs->image['coverImage'] = base_url($value->path.$value->filename);
                        }
                        if($value->grpType=='contentImage'){
                            $rs->image['contentImage'] = base_url($value->path.$value->filename);
                        }
                        
                    }
                    //$data['image'] = base_url($file['path'].$file['filename']);
                }
            }
           
            $data['info'] = $info;
            //arr($data['info']);exit();
            $this->load->view('hero', $data); 
            
        }
       

        // print"<pre>";print_r($data);exit();
    }
    public function social()
    {
        $input['type']='social';
        $input['active']='1';
        $input['recycle']='0';
        $input['social']='social';

        $info = $this->banner_m->get_list($input)->result();
       
        
        if ( !empty($info) ) {
            foreach ( $info as $key=>&$rs ) {
                $input_u['grpContent'] = $this->_grpContent;
                $input_u['contentId'] = $rs->bannerId;
                $file_ = $this->banner_m->get_uplode($input_u)->row();
                if (!empty($file_) && is_file($file_->path.$file_->filename)) {
                    $rs->image = base_url($file_->path.$file_->filename);
                
                }
            }
           
            $data['info'] = $info;
            $this->load->view('social', $data); 
            
        }
       

        // print"<pre>";print_r($data);exit();
    }
}
