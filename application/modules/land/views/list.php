<!-- Article Starts -->
          <?php if(!empty($info)){ foreach ($info as $key => $rs) { ?>
         
        <!-- Project Starts -->
        <div class="col s12 m12 l12 xl12" style="padding-bottom: 25px;">
          <!-- Picture Starts -->
           <div class="col s12 m6 l6 xl6 profile-picture personal-info">

            <div class="container_blog">
              <a href="<?php echo site_url("land/detail/{$rs['linkId']}");?>">
              <img src="<?php echo $rs['image'] ?>" alt="Avatar" class="responsive-img image_blog">
              <div class="overlay_blog">
                <div class="text_blog"><?php echo $rs['title'] ?></div>
              </div>
            </a>
            </div>
          
           </div>
          <!-- Picture Ends -->
          <div class="col s12 m6 l6 xl6 personal-info ">
            <h6 class="second-font left-align"><a href="<?php echo site_url("land/detail/{$rs['linkId']}");?>" style="color: #fa5b0f;"><?php echo $rs['title'] ?></a></h6>
            <p class="left-align"><?php echo iconv_substr($rs['excerpt'],0,90, "UTF-8").'...'; ?><span class="read-more"><a href="<?php echo site_url("land/detail/{$rs['linkId']}");?>" >อ่านต่อ</a></span><br>สถานที่ : <?php echo $rs['location'] ?></p>
             <p><h6 class="left-align"><b>ราคา : <?php echo price_th(str_replace(",","",$rs['price'])) ?></b></h6></p>
          
          </div>
          <div class="clearfix"></div>
          <div class="col s12 m6 l6 xl6 profile-picture personal-info" style="margin-top:10px;"> 
            <a href="<?php echo site_url("land/detail/{$rs['linkId']}");?>" class="col s12 m12 l12 xl12 waves-effect waves-light btn font-weight-500">
             รายละเอียด</i>
           </a>
          </div> 
       </div>
       <!-- Project Ends -->
        
     <?php } } ?>
          
         <div style="padding-bottom: 30px;" class="clearfix"></div>

        <!-- pagination -->
        <?php echo $links ;?>