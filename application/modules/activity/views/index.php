<style type="text/css">

  
.blog-activity-index .topnav-article{
    padding-top: 50px;
    padding-bottom: 25px;
  }

  .blog-activity-index .topnav-article a {
    float: left;
    color: #000;
    text-align: center;
    padding: 12px 20px;
    text-decoration: none;
    font-size: 17px;
    margin: 2px;
    border-radius: 20px;
    background-color: #eceeef;
  }
   .blog-activity-index .topnav-article a.active {
      background-color: #f69522;
      color: white;
  }
  .blog-activity-index .blog-activity {
    padding-top: 25px;
}
  .blog-activity-index .blog-activity .single-post{
    border-radius: 20px;
  }
  .blog-activity-index .blog-activity .single-post {
      background-color: #6d6e72;
      color: #fff;
      
  }
  .blog-activity-index .blog-activity .single-post .title-blog{
    font-size: 20px;
  }
  .blog-activity-index .blog-activity .single-post .title-blog i{
    color: red;
    font-size: 24px;
  }

  .blog-activity-index .blog-activity .image-wrapper {
    padding-bottom: 5px;
    padding-top: 5px;
}
  .blog-activity-index .blog-activity .single-post .content-activity .c-left {
     
     padding-left: 30px;
     
  }
  .blog-activity-index .blog-activity .single-post .content-activity  .c-left .title {
      font-size: 20px;
      color: #F8EE1D;
      margin-bottom: 0px !important;
  }
  .blog-activity-index .blog-activity .single-post .content-activity  p {
    font-size: 16px;
    
}
.blog-activity-index .blog-activity .single-post .content-activity ul li {
    list-style: none;
    display: list-item;
    font-size: 16px;
}

.blog-activity-index .blog-activity .single-post .content-activity .c-rigth span {
    font-size: 20px;
}
.blog-activity-index .blog-activity .single-post .content-activity .c-rigth  .price {
    padding-left: 35px;
}

</style>

<section class="blog-activity-index">
  <div class="container">
    <div class="row">

      <div class="col-xl-12 col-lg-12 col-md-12 col-ms-12"> 
        <div class="blog-posts">
                       <!--  <div class="title">
                            <h3>บทความกราฟฟิก</h3>
                            <div class="separator"></div>
                          </div> -->
                          <div class="row">
                           <div class=" col-sm-12 text-center">
                            <span class="topnav-article">

                              <!-- <?php foreach ($category as $key => $value) { ?> 
                                <a <?php if(!empty($nameLink) && $nameLink==$value['nameLink'] ){ ?> class="active" <?php } ?>  href="<?php echo site_url("activity/category/{$value['nameLink']}");?>"><?=$value['name']?></a>
                              <?php } ?> -->
                              <a <?php if(!empty($nameLink) && $nameLink=="เรียนสดแบบกลุ่ม" ){ ?> class="active" <?php } ?>  href="<?php echo site_url("activity");?>">เรียนสดแบบกลุ่ม</a>
                              <a <?php if(!empty($nameLink) && $nameLink=="one-to-one" ){ ?> class="active" <?php } ?>  href="<?php echo site_url("activity/category/one-to-one");?>">เรียนสดแบบตัวต่อตัว</a>
                              <a <?php if(!empty($nameLink) && $nameLink=="organization" ){ ?> class="active" <?php } ?>  href="<?php echo site_url("activity/category/organization");?>">เรียนสดในองค์กร</a>

                            </span>


                          </div>
                        </div>
                        <?php if(!empty($info)){ ?>
                         <div class="row">

                          <?php foreach ($info as $key => $rs) { ?>
                            <div class=" col-sm-2"></div>
                            <div class=" col-sm-8">
                            <div class="blog-activity">
                              <div class="single-post">
                                <div class="title-blog row">
                                   <div class=" col-sm-6"><?=$rs['date']?></div>
                                    <div class=" col-sm-6"><i class="fa fa-map-marker"></i> <?=$rs['location']?></div>
                                </div>
                                <div class="row">
                                  <div class="col-lg-12 col-md-12 col-ms-12 ">
                                    <div class="">
                                      <div class="image-wrapper">

                                        <img src="<?php echo $rs['image']; ?>" alt="Blog Image">


                                      </div>  
                                    </div><!-- single-post -->

                                  </div>
                                  <div class="col-lg-12 col-md-12 col-ms-12 ">
                                    <div class="content-activity row">
                                      <div class="col-lg-7 col-md-12 col-ms-7 c-left">
                                        <span class="title"><?php echo $rs['title'];?></span>
                                        <p><?php echo $rs['excerpt'];?></p>
                                        <ul>
                                          <li>วัน : <?php echo $rs['date'];?></li>
                                          <li>เวลา : <?php echo $rs['time'];?></li>
                                          <li>สถานที่ : <?php echo $rs['location'];?></li>
                                        </ul>
                                        
                                      </div>
                                      <div class="col-lg-5 col-md-12 col-ms-5 c-rigth">
                                        <?php if($rs['promotion']!=""){ ?>
                                          <ul class="price">
                                             <li>จำนวนที่นั่ง : เหลือ <?php echo number_format($rs['userNum']);?> </li>
                                            <li>ราคา : <span style="font-size:18px; text-decoration: line-through;text-decoration-color: red;"><?php echo number_format($rs['price']);?> </span> บาท</li>
                                            <li>โปรโมชั่น : <?php echo number_format($rs['promotion']);?> บาท</li>
                                          </ul>
                                        <?php }else{ ?>
                                          <ul class="price">
                                             <li>จำนวนที่นั่ง : เหลือ <?php echo number_format($rs['userNum']);?> </li>
                                            <li>ราคา : <?php echo number_format($rs['price']);?> บาท</li>

                                          </ul>
                                        <?php } ?>
                                        <ul>
                                            <center><a  class="button-click" href="<?php echo site_url("activity/detail/{$rs['linkId']}");?>"><span style="color: #f7f007">
                                            คลิก!! ดูรายละเอียด </span><img src="<?php echo base_url('assets/website/images/icon/click.png')?>" style="width: 30px">
                                          </a>
                                        </center>
                                        </ul>
                                      </div>
                                </div><!-- single-post -->
                              </div>
                            </div>
                          </div>
                        </div>

                        </div>
                        <div class=" col-sm-2"></div>
                        <div class="clearfix"></div>
                      <?php } ?>


                    </div><!-- row -->
                    <div class="text-center " style="padding-top: 25px">
                      <?php echo $links ;?>   
                    </div>
                  <?php }else{ ?>
                    <div class="text-center"  style="padding-bottom: 25px">

                      <h4>ไม่พบข้อมูล</h4>

                    </div>
                  <?php } ?>

                </div><!-- blog-posts -->
              </div><!--col-lg-4 -->
              <!-- <div id="get_more"></div> -->

            </div>  
          </div>
    </section><!-- section -->