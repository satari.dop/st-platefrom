<style type="text/css">

  
.blog-activity-index .topnav-article{
    padding-top: 50px;
    padding-bottom: 25px;
  }

  .blog-activity-index .topnav-article a {
    float: left;
    color: #000;
    text-align: center;
    padding: 12px 20px;
    text-decoration: none;
    font-size: 17px;
    margin: 2px;
    border-radius: 20px;
    background-color: #eceeef;
  }
   .blog-activity-index .topnav-article a.active {
      background-color: #f69522;
      color: white;
  }
  .blog-activity-index .blog-activity {
    padding-top: 25px;
}
  .blog-activity-index .blog-activity .single-post{
    border-radius: 20px;
  }
  .blog-activity-index .blog-activity .single-post {
      background-color: #6d6e72;
      color: #fff;
      
  }
  .blog-activity-index .blog-activity .single-post .title-blog{
    font-size: 20px;
  }
  .blog-activity-index .blog-activity .single-post .title-blog i{
    color: red;
    font-size: 24px;
  }

  .blog-activity-index .blog-activity .image-wrapper {
    padding-bottom: 5px;
    padding-top: 5px;
}
  .blog-activity-index .blog-activity .single-post .content-activity .c-left {
     
     padding-left: 30px;
     
  }
  .blog-activity-index .blog-activity .single-post .content-activity  .c-left .title {
      font-size: 20px;
      color: #F8EE1D;
      margin-bottom: 0px !important;
  }
  .blog-activity-index .blog-activity .single-post .content-activity  p {
    font-size: 16px;
    
}
.blog-activity-index .blog-activity .single-post .content-activity ul li {
    list-style: none;
    display: list-item;
    font-size: 16px;
}

.blog-activity-index .blog-activity .single-post .content-activity .c-rigth span {
    font-size: 20px;
}
.blog-activity-index .blog-activity .single-post .content-activity .c-rigth  .price {
    padding-left: 35px;
}

</style>

<section class="blog-activity-index">
  <div class="container">
    <div class="row">

      <div class="col-xl-12 col-lg-12 col-md-12 col-ms-12"> 
        <div class="blog-posts">

                  <div class="row">
                   <div class=" col-sm-12 text-center">
                    <span class="topnav-article">

                    
                        <a <?php if(!empty($nameLink) && $nameLink=="เรียนสดแบบกลุ่ม" ){ ?> class="active" <?php } ?>  href="<?php echo site_url("activity");?>">เรียนสดแบบกลุ่ม</a>
                        <a <?php if(!empty($nameLink) && $nameLink=="one-to-one" ){ ?> class="active" <?php } ?>  href="<?php echo site_url("activity/category/one-to-one");?>">เรียนสดแบบตัวต่อตัว</a>
                        <a <?php if(!empty($nameLink) && $nameLink=="organization" ){ ?> class="active" <?php } ?>  href="<?php echo site_url("activity/category/organization");?>">เรียนสดในองค์กร</a>

                      </span>


                    </div>
                  </div>
                  <?php if(!empty($info)){ ?>
                   <div style="padding-bottom: 25px;">
                      <?php echo html_entity_decode($info); ?>
                   </div>
                 <?php }else{ ?>
                  <div class="text-center"  style="padding-bottom: 25px">

                    <h4>ไม่พบข้อมูล</h4>

                  </div>
                <?php } ?>

              </div><!-- blog-posts -->
            </div><!--col-lg-4 -->
            <!-- <div id="get_more"></div> -->

          </div>  
        </div>
</section><!-- section -->