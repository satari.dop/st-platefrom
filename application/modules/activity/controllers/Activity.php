<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends MX_Controller {
    
    private $_grpContent = 'activity';

    public function __construct() 
    {
        parent::__construct();
        
        $this->load->model('category/category_m');
        $this->load->model('activity_m');
         $this->load->model('seo_m');
    }
    
    public function index()
    {
        Modules::run('track/front','');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('activity/index/');
        $this->session->set_userdata('urlreffer', $urlreffer);

        $this->load->module('front');
        $input_ = $this->input->get();
        //$input['nameLink'] ="เรียนสดแบบกลุ่ม";
        $data['nameLink']="เรียนสดแบบกลุ่ม";
        
        $input['grpContent'] = $this->_grpContent;

        $input_c['categoryType'] = $this->_grpContent;
        $data['category'] = $this->category_m->get_rows($input_c)->result_array();

        ///pagination///
        
        $uri='activity';  
        $total=$this->activity_m->get_count($input);
        $segment=3;
        $per_page = 5;
        $data["links"] = $this->pagin2($uri, $total, $segment, $per_page);
        ///pagination///
        
        $page = ($this->input->get('per_page')) ? ($per_page*($this->input->get('per_page')-1)) : 0;

        $input['length']=$per_page;
        $input['start']=$page;
        $data['info'] = "";
        $info=$this->activity_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ( $info as $key=>&$rs ) {
                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = !empty($rs['activityId'])?$rs['activityId']:'';
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $rs['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->activity_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }

                if($rs['startDate']==$rs['endDate']){
                    $rs['date']=date_language($rs['startDate'],FALSE,'th');
                }else{
                    $rs['date']=date_language($rs['startDate'],FALSE,'th').' - '.(date_language($rs['endDate'],FALSE,'th'));
                }

                $rs['time']=date('H:i',strtotime($rs['startTime'])).' - '.date('H:i',strtotime($rs['endTime'])).' น.';
                
            }
         
            $data['info'] = $info;
           // arr($data['info']);exit();
           
        }
       
        $data['activity_act'] = 'active';
       
        $data['contentView'] = "activity/index";
        $data['pageHeader'] = "กิจกรรม";
        
        $input_seo['grpContent'] ='seo_activity';
        $seo = $this->seo_m->get_rows($input_seo);
        $seo_ = $seo->row();

        //arr($seo_);exit();

        $imgSeo = config_item('metaOgImage');
        $metaTitle="กิจกรรม";
        $metaDescription="กิจกรรม";
        $metaKeyword="กิจกรรม";
        
        if(!empty($seo_)){           
            $input_seo['contentId'] = $seo_->seoId;
            $file = $this->seo_m->get_uplode($input_seo)->row_array();
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $imgSeo = base_url($file['path'].$file['filename']);
            }
           
            $metaTitle=$seo_->metaTitle;
            $metaDescription=$seo_->metaDescription;
            $metaKeyword=$seo_->metaKeyword;
        }
        
        //echo CI_VERSION ; exit();

        $data['metaTitle'] = $metaTitle;
        $data['metaDescription_'] = $metaDescription;
        $data['metaKeyword_'] = $metaKeyword;

        $share['ogTitle']=$metaTitle;
        $share['ogDescription']=$metaDescription;
        $share['ogUrl']= 'home';
        $share['ogImage']= $imgSeo;
        $this->_social_share($share);

        $this->front->layout($data);
        
    }

    public function category($categoryId)
    {

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('activity/category/'.$categoryId);
        $this->session->set_userdata('urlreffer', $urlreffer);

        Modules::run('track/front','');

        

        $this->load->module('front');
        $input_ = $this->input->get();
        
        $input['grpContent'] = $this->_grpContent;
        $input['nameLink'] = $categoryId;
        $data['nameLink']=$categoryId;

       
        $cat=$categoryId;
        if($categoryId=="one-to-one"){
            $cat="one_to_one";
        }
        $query = $this->db
                        ->from('config')
                        ->where('type', 'activity')
                        ->where('variable', $cat)
                        ->get()
                        ->row_array();
        if(!empty($query)){
            $data['info']=$query['value'];
        }
        // $input_c['categoryType'] = $this->_grpContent;
        // $data['category'] = $this->category_m->get_rows($input_c)->result_array();

        // ///pagination///
        
        // $uri='activity/category/'.$categoryId;
        // $total=$this->activity_m->get_count($input);
        // $segment=3;
        // $per_page = 5;
        // $data["links"] = $this->pagin2($uri, $total, $segment, $per_page);
        // ///pagination///
        
        // $page = ($this->input->get('per_page')) ? ($per_page*($this->input->get('per_page')-1)) : 0;

        // $input['length']=$per_page;
        // $input['start']=$page;
        // $data['info'] = "";
        // $info=$this->activity_m->get_rows($input)->result_array();
        // if (!empty($info)) {
        //     foreach ( $info as $key=>&$rs ) {
        //         $input['grpContent'] = $this->_grpContent;
        //         $input['contentId'] = !empty($rs['activityId'])?$rs['activityId']:'';
        //         $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
        //         $rs['image'] = base_url("assets/images/no_image3.png");
                
        //         $file = $this->activity_m->get_uplode($input)->row_array();
               
        //         if (!empty($file) && is_file($file['path'].$file['filename'])) {
        //             $rs['image'] = base_url($file['path'].$file['filename']);
        //         }

        //         if($rs['startDate']==$rs['endDate']){
        //             $rs['date']=date_language($rs['startDate'],FALSE,'th');
        //         }else{
        //             $rs['date']=date_language($rs['startDate'],FALSE,'th').' - '.(date_language($rs['endDate'],FALSE,'th'));
        //         }

        //         $rs['time']=date('H:i',strtotime($rs['startTime'])).' - '.date('H:i',strtotime($rs['endTime'])).' น.';
                
        //     }
         
        //     $data['info'] = $info;
        //    // arr($data['info']);exit();
           
        // }
       
        $data['activity_act'] = 'active';
       
        $data['contentView'] = "activity/category2";
        $data['pageHeader'] = "กิจกรรม";
        
        $input_seo['grpContent'] ='seo_activity';
        $seo = $this->seo_m->get_rows($input_seo);
        $seo_ = $seo->row();

        //arr($seo_);exit();

        $imgSeo = config_item('metaOgImage');
        $metaTitle="กิจกรรม";
        $metaDescription="กิจกรรม";
        $metaKeyword="กิจกรรม";
        
        if(!empty($seo_)){           
            $input_seo['contentId'] = $seo_->seoId;
            $file = $this->seo_m->get_uplode($input_seo)->row_array();
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $imgSeo = base_url($file['path'].$file['filename']);
            }
           
            $metaTitle=$seo_->metaTitle;
            $metaDescription=$seo_->metaDescription;
            $metaKeyword=$seo_->metaKeyword;
        }
        
        //echo CI_VERSION ; exit();

        $data['metaTitle'] = $metaTitle;
        $data['metaDescription_'] = $metaDescription;
        $data['metaKeyword_'] = $metaKeyword;

        $share['ogTitle']=$metaTitle;
        $share['ogDescription']=$metaDescription;
        $share['ogUrl']= 'home';
        $share['ogImage']= $imgSeo;
        $this->_social_share($share);

        $this->front->layout($data);
        
    }

    public function calendar() {
         
        $input['grpContent'] = $this->_grpContent;
        
        $info=$this->activity_m->get_rows($input)->result_array();
        $colors = explode(',', '#594a2d,#660057,#6e8cff,#3d0c4e,#9ce6ae,#009e78,#001f8f,#ff7852,#ebff26,#0073e6,#f05eff,#00decc');
        //backgroundColor =>sprintf('#%06X', mt_rand(0, 0xFFFFFF)),
         if ( !empty($info) ) {
            $info_ = array();
            foreach ( $info as $key=>&$rs ) {
                if($rs['startDate']==$rs['endDate']){
                    $date=date_language($rs['startDate'],FALSE,'th');
                }else{
                    $date=date_language($rs['startDate'],FALSE,'th').' ถึง '.(date_language($rs['endDate'],FALSE,'th'));
                }

                 $time=date('H:i',strtotime($rs['startTime'])).' - '.date('H:i',strtotime($rs['endTime'])).' น.';
                 $linkId = str_replace(" ","-",$rs['title_link']);
                 $input_arrays[]= array(location=>$rs['location'],time=>$time,date=>$date,allDay => false, title => $rs['title'], start => $rs['startDate'].'T'.$rs['startTime'], end => $rs['endDate'].'T'.$rs['endTime'] , url => site_url('activity/detail/'.$linkId));
            }
           
            if ( !empty($info) ) {
                echo json_encode($input_arrays);
              
            }
         }

    }

    

    public function tags($tagsId)
    {
        $this->load->module('front');
        $input = $this->input->get();
        $input['grpContent'] = $this->_grpContent;
        $input['tagsId'] = $tagsId;
        
        $input_c['categoryType'] = $this->_grpContent;
        $data['category'] = $this->category_m->get_rows($input_c)->result_array();

        $input_t['tagsType'] = $this->_grpContent;
        $data['tags_'] = $this->tags_m->get_rows($input_t)->result_array();

        ///pagination///
        $uri='activity/tags/'.$tagsId;
        $total=$this->activity_m->get_count($input);
        $segment=3;
        $per_page = 10;
        $data["links"] = $this->pagin($uri, $total, $segment, $per_page);
        ///pagination///
        
        $page = ($this->input->get('per_page')) ? ($per_page*($this->input->get('per_page')-1)) : 0;

        $input['length']=$per_page;
        $input['start']=$page;

        $info=$this->activity_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ( $info as $key=>&$rs ) {
                $rs['linkId'] = str_replace(" ","-",$rs['title']);
                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $rs['contentId'];
                $rs['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->activity_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }
                $rs['tags']="";
                if($rs['tagsId']!=''){
                     $tags=explode(',', $rs['tagsId']);
                     $rstags  = array( );
                     foreach ($tags as $key => $value) {
                        $input_t['tagsID'] = $value;
                        $t=$this->tags_m->get_rows($input_t)->row_array();
                        $rstags[]=$t['tagsName'];
                     }

                     $rs['tags']=implode(',', $rstags);
                     
                }

             

                

            }
         
            $data['info'] = $info;
           
        }
        $data['contentHeader'] = "activity/header";
        $data['contentView'] = "activity/index";
        $data['sideBarView'] = "activity/sideBar2";
        $this->front->layout($data);
        // $data['info'] = '';
        // $this->load->view('index', $data); 
    }

    public function detail($id=0)
    {
        
        $this->load->module('front');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('activity/detail/').$id;
        $this->session->set_userdata('urlreffer', $urlreffer);
       
        $input['title_link'] = str_replace("-"," ",$id);
        //$input['keyword']=
        $input['grpContent'] = $this->_grpContent;
        $data['keyword'] = "";
        $data['name']=$input['title_link'];

        // $input_c['categoryType'] = $this->_grpContent;
        // $data['category'] = $this->category_m->get_rows($input_c)->result_array();

        // $input_t['tagsType'] = $this->_grpContent;
        // $data['tags'] = $this->tags_m->get_rows($input_t)->result_array();

        $info=$this->activity_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "สถาบันออนไลน์");

        //print"<pre>";print_r($info); exit(); 
        if (!empty($info)) {
            //foreach ( $info as $key=>&$rs ) {

                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $info['activityId'];
                $info['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->activity_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $info['image'] = base_url($file['path'].$file['filename']);
                    $imageSeo = base_url($file['path'].$file['filename']);
                }

                $uplode = $this->activity_m->get_uplode($input)->result_array();
               
                if (!empty($uplode)){
                    $data['image_g'] = array();
                    foreach ($uplode as $key => $value) {
                        
                        if($value['grpType']=='galleryImage'){
                            $coverImage_path=$value['path'];
                            $coverImage_filename=$value['filename'];
                            $data['image_g'][] = base_url($coverImage_path.$coverImage_filename);
                        }
                    }
                    //$data['JSON_img_arr']=json_encode($data['image_g']);
                }

                if($info['startDate']==$info['endDate']){
                    $info['date']=date_language($info['startDate'],FALSE,'th');
                }else{
                    $info['date']=date_language($info['startDate'],FALSE,'th').' ถึง '.(date_language($info['endDate'],FALSE,'th'));
                }

                $info['time']=date('H:i',strtotime($info['startTime'])).' - '.date('H:i',strtotime($info['endTime'])).' น.';
                

            $data['info'] = $info;

           // }

        }
        $this->activity_m->plus_view($info['contentId']);
        Modules::run('track/front',$info['contentId']);
        
        $data['activity_act'] = 'active';
       
        $data['contentView'] = "activity/detail";
        $data['pageHeader'] = $info['metaTitle'];
        
        $data['metaTitle'] = $info['metaTitle'];
        $data['metaDescription_'] = $info['metaDescription'];
        $data['metaKeyword_'] = $info['metaKeyword'];

        $share['ogTitle']=$info['metaTitle'];
        $share['ogDescription']=$info['metaDescription'];
        $share['ogUrl']= 'activity/detail/'.$id;
        $share['ogImage']= $imageSeo;
        $this->_social_share($share);
        $this->front->layout($data);
    }

    public function relate($contentId=0,$categoryId=0)
    {
        $input['grpContent'] = $this->_grpContent;
        $input['length']=10;
        $input['start']=0;
        $input['categoryId']=$categoryId;
        $input['exclude']=$contentId;
        
        $info = $this->activity_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ($info as &$rs) {
                $input_u['contentId'] = $rs['activityId'];
                $input_u['grpContent'] = $this->_grpContent;
                $uplode = $this->activity_m->get_uplode($input_u)->result_array();
                $rs['linkId'] = str_replace(" ","-",$rs['title']);
                $rs['image']=base_url("assets/images/no_image2.png");
                if (!empty($uplode)){
                    foreach ($uplode as $key => $value) {
                        if($value['grpType']=='coverImage'){
                            $coverImage_path=$value['path'];
                            $coverImage_filename=$value['filename'];
                            $rs['image'] = base_url($coverImage_path.'thumbnail/'.$coverImage_filename);
                        }
                    }
                }
                
                $rs['createDate'] = date_language($rs['createDate'], TRUE, 'th');
            }
        }
        $data['info'] = $info;
       //print"<pre>"; print_r($data['info']); exit();
        if (!empty($info)) $this->load->view('relate', $data);
    }

    public function recent()
    {
        $input['grpContent'] = $this->_grpContent;
        $input['length']=1;
        $input['start']=0;
        $input['recent']="recent";
        //$input['exclude']=$contentId;
        
        $rs = $this->activity_m->get_rows($input)->row_array();
      
        if (!empty($rs)) {
            //foreach ($info as &$rs) {
                
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);

                $input_u['contentId'] = $rs['activityId'];
                $input_u['grpContent'] = $this->_grpContent;

                

                $rs['image'] = base_url("assets/images/no_image2.png");
                
                $file = $this->activity_m->get_uplode($input_u)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }

                if($rs['startDate']==$rs['endDate']){
                    $rs['date']=date_language($rs['startDate'],FALSE,'th');
                }else{
                    $rs['date']=date_language($rs['startDate'],FALSE,'th').' ถึง '.(date_language($rs['endDate'],FALSE,'th'));
                }

                $rs['time']=date('H:i',strtotime($rs['startTime'])).' - '.date('H:i',strtotime($rs['endTime'])).' น.';
                
                
                
            //}
        }
        $data['info_recent'] = $rs;
        //print"<pre>"; print_r($data['info_recent']); exit();
        if (!empty($rs)) $this->load->view('recent', $data);
    }

    public function pagin($uri, $total, $segment, $per_page = 30) {

        $config['base_url'] = site_url($uri);
        $config['per_page'] = $per_page;
        $config['total_rows'] = $total;
        $config['uri_segment'] = $segment;
        //$config['num_links'] = $num_links;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination center-align">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE; //'<span aria-hidden="true">«</span>';
        $config['last_link'] = FALSE; //'<span aria-hidden="true">»</span>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span aria-hidden="true"><i class="fa fa-angle-left"></i></span>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    }  

    public function pagin2($uri, $total, $segment, $per_page = 30) {

        $config['base_url'] = site_url($uri);
        $config['per_page'] = $per_page;
        $config['total_rows'] = $total;
        $config['uri_segment'] = $segment;
        //$config['num_links'] = $num_links;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = FALSE; //'<span aria-hidden="true">«</span>';
        $config['last_link'] = FALSE; //'<span aria-hidden="true">»</span>';
        // $config['first_tag_open'] = '<a>';
        // $config['first_tag_close'] = '</a>';
        $config['prev_link'] = '&laquo;';
        // $config['prev_tag_open'] = '<a>';
        // $config['prev_tag_close'] = '</a>';
        $config['next_link'] = '&raquo;';
        // $config['next_tag_open'] = '<a>';
        // $config['next_tag_close'] = '</a>';
        // $config['last_tag_open'] = '<a>';
        // $config['last_tag_close'] = '</a>';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';
        // $config['num_tag_open'] = '<li>';
        // $config['num_tag_close'] = '</li>';

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    
}
