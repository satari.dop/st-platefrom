<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Search_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*,b.name,c.username')
                        ->from('content a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->join('user c', 'a.createBy = c.userId', 'left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*,b.name')
                        ->from('content a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {
        //arr($param);exit();
        if ($param['grpContent']!=""){ 
            $this->db->where('a.grpContent', $param['grpContent']);
        }else{
            $this->db->where_in('a.grpContent',array('house','rental','land'));
        }

        $this->db->where('a.price <=',$param['max_price']);
        $this->db->where('a.price >=',$param['min_price']);

        $this->db->where('a.recycle',0);
        $this->db->where('a.active',1);


        if ( isset($param['keyword']) ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }

        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $param['createStartDate'] = $param['createStartDate']." 00:00";
            $param['createEndDate'] = $param['createEndDate']." 23:59";
            
            $this->db->where('a.createDate >=', $param['createStartDate']);
            $this->db->where('a.createDate <=', $param['createEndDate']);

        }
        
        if (isset($param['categoryId'])){ 
           if ($param['categoryId']!=""){ 
                $this->db->where('a.categoryId', $param['categoryId']);
            }
        }
        if ($param['location']!=""){ 
                $this->db->where('a.location', $param['location']);
        }
       
        if ( isset($param['contentId']) ) 
             $this->db->where('a.contentId', $param['contentId']);
         if ( isset($param['statusBuy']) ) 
             $this->db->where('a.statusBuy', $param['statusBuy']);

        if ( !in_array($this->router->method, array("profile","check_password","check_email")))
            $this->db->where('a.contentId !=', $this->session->content['contentId']);
        
        if (isset($param['exclude'])) {
            $this->db
                    ->where('a.contentId !=', $param['exclude']);
        }

        $this->db->order_by('price', 'ASC');
        //$this->db->order_by('createDate', 'DESC');
        //$this->db->order_by('updateDate', 'DESC');


    }
    
    public function get_by_id($id)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('content a')
                        ->join('category b', "a.categoryId = b.categoryId", 'left')
                        ->where('a.contentId', $id)
                        ->get()
                        ->row_array();
        return $query;
    }
    
    public function plus_view($id)
    {
        $sql = "UPDATE content SET view = (view+1) WHERE contentId=?";
        $this->db->query($sql, array($id));
    }
    
    public function get_unread($readNews)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('news a')
                        ->where('a.newsActive', 1)
                        ->where('a.newsRecycle', 0)
                        ->where('a.approve', 1)        
                        ->where_not_in('a.newsId', $readNews)
                        ->order_by('a.newsDateCreate', 'desc')
                        ->get()
                        ->result_array();
        return $query;
    }

    public function get_uplode($param) 
    {
        $this->_condition_uplode($param);
        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('upload a')
                        ->join('upload_content b', 'a.uploadId = b.uploadId', 'left')
                        ->get();
        return $query;
    }

    private function _condition_uplode($param) 
    {
        if ($param['grpContent']!=""){ 
            $this->db->where('a.grpContent', $param['grpContent']);
        }
        if ( isset($param['contentId']) ) 
             $this->db->where('b.contentId', $param['contentId']);

        if ( isset($param['grpType']) ) 
             $this->db->where('b.grpType', $param['grpType']);

    }
        
}