<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MX_Controller {
    
    

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('search_m');
       
    }
    
   

    public function index()
    {
        Modules::run('track/front','');
        $this->load->module('front');
        $input = $this->input->get();
        $input['grpContent'] = $input['type'];
        $input['statusBuy'] = 0;

        

        $input['min_price'] = substr($input['range'], 0 ,strpos($input['range'], ";"));
        $input['max_price'] = substr($input['range'],strpos($input['range'], ";")+1);
        

        ///pagination///
        $uri="search?type={$input['type']}&location={$input['location']}&range={$input['range']}";
        $total=$this->search_m->get_count($input);
        $segment=3;
        $per_page = 10;
        $data["links"] = $this->pagin($uri, $total, $segment, $per_page);
        ///pagination///
        
        $page = ($this->input->get('per_page')) ? ($per_page*($this->input->get('per_page')-1)) : 0;

        $input['length']=$per_page;
        $input['start']=$page;

        $info=$this->search_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ( $info as $key=>&$rs ) {
                $input['grpContent'] = $input['type'];
                $input['contentId'] = $rs['contentId'];
                $rs['linkId'] = str_replace(" ","-",$rs['title']);
                $rs['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->search_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }

                if($rs['grpContent']=='house'){
                    $rs['typeName']='บ้านมือสอง';
                }elseif ($rs['grpContent']=='rental') {
                    $rs['typeName']='บ้านเช่า';
                }elseif ($rs['grpContent']=='land') {
                    $rs['typeName']='ที่ดิน';
                }
                

            }
         
            $data['info'] = $info;
           
        }
       // print"<pre>"; print_r($data['info']); exit();
        
        $data['contentHeader'] = "search/header";
        $data['contentView'] = "search/list";
        $data['sideBarView'] = "search/sideBar";
        

        $share['ogTitle']="";
        $share['ogDescription']="";
        $share['ogUrl']= '';
        $share['ogImage']= config_item('metaOgImage');
        $this->_social_share($share);

        $this->front->layout_blog($data);
        // $data['info'] = '';
        // $this->load->view('index', $data); 
    }

    public function getTrack(){
        $input = $this->input->post(null, true);
        Modules::run('track/front',$input['contentId'],'detail',$this->_grpContent);
        // print_r($input);exit();
    }

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    } 

    public function pagin($uri, $total, $segment, $per_page = 30) {

        $config['base_url'] = site_url($uri);
        $config['per_page'] = $per_page;
        $config['total_rows'] = $total;
        $config['uri_segment'] = $segment;
        //$config['num_links'] = $num_links;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination center-align">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE; //'<span aria-hidden="true">«</span>';
        $config['last_link'] = FALSE; //'<span aria-hidden="true">»</span>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span aria-hidden="true"><i class="fa fa-angle-left"></i></span>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    } 

    
}
