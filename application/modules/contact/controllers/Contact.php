<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("contact_m");
        $this->load->model('front/front_m');
    }

    public function index($id = "") {
        $this->load->module('front');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('contact/index/');
        $this->session->set_userdata('urlreffer', $urlreffer);
        
        $data['frmAction'] = site_url("contact/save");
        $data['contact_act']="active";
        $data['contentView'] = 'contact/index';
        $this->front->layout($data);
    }
    public function save()
    {

        $input = $this->input->post();

	    $ip = $_SERVER['REMOTE_ADDR'];

	    
    	if($this->input->post('robot') != null){
    		$captcha = "ok";
    		$responseKeys["success"] = 1;
    	} else {
    		$captcha = false;
    		$responseKeys["success"] = false;
    	}


	    if(intval($responseKeys["success"]) !== 1||!$captcha) {
		    echo json_encode(array('info_txt'=>'error','msg' => '* Captcha error','msg2'=>'กรุณาลองใหม่อีกครั้ง!'));
	    } else {
        
	        

	        $value = $this->_build_data($input);
	        $result = $this->contact_m->insert($value);
	        if ( $result ) {

            $data=$value;
            $email=$value['email'];

            $input['type'] = 'mail';

            $input['variable'] = 'SMTPserver';
            $SMTPserver=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPusername';
            $SMTPusername=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPpassword';
            $SMTPpassword=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'SMTPport';
            $SMTPport=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderEmail';
            $senderEmail=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'senderName';
            $senderName=$this->front_m->getConfig($input)->row();

            $input['variable'] = 'mailDefault';
            $mailDefault=$this->front_m->getConfig($input)->row();
            
            $textEmail = $this->load->view('contact/mailer_form.php',$data , TRUE);
                          
                           

              require 'application/third_party/phpmailer/PHPMailerAutoload.php';
              $mail = new PHPMailer;

              //$mail->SMTPDebug = 3;                               // Enable verbose debug output

              $mail->isSMTP();                                      // Set mailer to use SMTP
              $mail->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
              $mail->SMTPAuth = true;                               // Enable SMTP authentication
              $mail->Username = $SMTPusername->value;                // SMTP username
              $mail->Password = $SMTPpassword->value;                          // SMTP password
              $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
              $mail->Port = $SMTPport->value;                                           // TCP port to connect to
              $mail->CharSet = 'UTF-8';
              $mail->From = $senderEmail->value;
              $mail->FromName = $senderName->value;
              $mail->addAddress($email);               // Name is optional
              
              $mail->isHTML(true);                                  // Set email format to HTML

              $mail->Subject = $email.' : ติดต่อสอบถาม';
              $mail->Body    = $textEmail;
              $mail->AltBody = $textEmail;
              $mail->send();

              $textEmail2 = $this->load->view('contact/mailer_form_admin.php',$data , TRUE);

                $mail2 = new PHPMailer;
                
                $mail2->isSMTP();                                      // Set mailer to use SMTP
                $mail2->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
                $mail2->SMTPAuth = true;                               // Enable SMTP authentication
                $mail2->Username = $SMTPusername->value;                // SMTP username
                $mail2->Password = $SMTPpassword->value;                          // SMTP password
                $mail2->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail2->Port = $SMTPport->value;                                           // TCP port to connect to
                $mail2->CharSet = 'UTF-8';
                $mail2->From = $senderEmail->value;
                $mail2->FromName = $senderName->value;
                $mail2->addAddress($mailDefault->value);               // Name is optional

                
                $mail2->isHTML(true);                                  // Set email format to HTML

                $mail2->Subject = $email.' : ติดต่อสอบถาม';
                $mail2->Body    = $textEmail2;
                $mail2->AltBody = $textEmail2;
                $mail2->send();

	            $resp_msg = array('info_txt'=>"success",'msg'=>'ส่งข้อความสำเร็จ','msg2'=>'กรุณารอสักครู่...');
	                echo json_encode($resp_msg);
	                return false;
	        } else {
	            $resp_msg = array('info_txt'=>"error",'msg'=>'ส่งข้อความไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
	            echo json_encode($resp_msg);
	            return false;
	        }
	    }
    }
    private function _build_data($input) {
       
        
        $value['fname'] = $input['fname'];
        $value['lname'] = $input['lname'];
        $value['phone'] = $input['tel'];
        $value['email'] = $input['email'];
        $value['detail'] = $input['massage'];
        $value['createDate'] = db_datetime_now();
        $value['updateDate'] = db_datetime_now();
     
      
        return $value;
    }

}
