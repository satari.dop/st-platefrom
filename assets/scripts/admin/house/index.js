"use strict";
$(document).ready(function () {
    dataList.DataTable({
        language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        bFilter: false,
        ajax: {
            url: "admin/"+controller+"/data_index",
            type: 'POST',
            data: {'csrfToken': get_cookie('csrfCookie'), frmFilter:(function(){return $("#frm-filter").serialize()})},
        },
        order: [[3, "desc"]],
        pageLength: 10,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "title", className: "", orderable: true},
            {data: "excerpt",width: "25%", className: "", orderable: true},

            {data: "createDate", width: "110px", className: "", orderable: true},
            {data: "updateDate", width: "110px", className: "", orderable: true},
            {data: "statusBuy",width: "100px", className: "", orderable: true},
            {data: "active", width: "65px", className: "text-center", orderable: false},
            {data: "action", width: "50px", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
             $('.statusPicker').selectpicker({width: '100%'});    
        }
    })

    $(document).on('change','.statusPicker', function(){
        if(jQuery.type($(this).data('method')) != "undefined"){
            var style
            $('#overlay-box').removeClass('hidden');
            switch ($(this).val())
            { 
                case "0" : style = 'btn-default'; break; //ยังไม่ขาย
                case "1" : style = 'btn-danger'; break; //ขายแล้ว
                default : style = 'btn-default'; 
            }        
            $(this).selectpicker('setStyle', 'btn-danger btn-warning btn-success btn-info btn-default', 'remove')
            $(this).selectpicker('setStyle', style)
            $(this).selectpicker('setStyle', 'btn-flat', 'add')
            arrayId.push($(this).data('id'))
            console.log(arrayId);
            $.post($(this).data('method'), {id: arrayId, statusBuy: $(this).val(), csrfToken: csrfToken})
                    .done(function (data) {
                        $('#overlay-box').addClass('hidden');
                        if (data.success === true) {
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                        } else if (data.success === false) {
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                            errorToggle = true
                            bootstrapToggle.bootstrapToggle('toggle')
                            errorToggle = false
                        }
                        arrayId = []
                    })
                    .fail(function () {
                        $('#overlay-box').addClass('hidden');
                        toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "ห้องสมุดศูนย์มานุษยวิทยาสิรินธร")
                        errorToggle = true
                        bootstrapToggle.bootstrapToggle('toggle')
                        errorToggle = false
                        arrayId = []
                    })
        }
    })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
