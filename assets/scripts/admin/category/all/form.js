"use strict";

$(document).ready(function () {
    $('.form-horizontal').validate();
    
    $('.select2').change(function () {
        $('.form-horizontal').validate().element('.select2');
    })
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
