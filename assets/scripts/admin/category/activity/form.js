"use strict";

$(document).ready(function () {
    //$('.form-horizontal').validate();
    $('.form-horizontal').validate({
        rules: {
            name: {
                remote: {
                    url: "admin/category/checkTitle",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, categoryType: function () {
                            return $('#input-catetory-type').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            }
        },
        messages: {
            name: {remote: 'ชื่อหมวดหมู่ซ้ำ'}
        }
        
    });
    
    $('.select2').change(function () {
        $('.form-horizontal').validate().element('.select2');
    })
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
