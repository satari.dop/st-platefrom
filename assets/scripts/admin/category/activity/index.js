"use strict";

$(document).ready(function () {
    dataList.DataTable({
        language: {url: siteUrl + "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        ajax: {
            url: siteUrl + "admin/"+controller+"/data_index",
            type: 'POST',
            data: {
                'categoryType' : categoryType,
                'csrfToken': get_cookie('csrfCookie')
            },
        },
        order: [[1, "asc"]],
        pageLength: 10,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "order", className: "", orderable: true, visible: false},
            {data: "name", className: "", orderable: false},
            {data: "excerpt", className: "", orderable: false},
            {data: "createDate", className: "", orderable: false},
            {data: "updateDate", className: "", orderable: false},
            {data: "active", width: "70px", className: "text-center", orderable: false},
            {data: "action", width: "100px", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
