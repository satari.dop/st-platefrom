"use strict";
$(document).ready(function () {
    
    dataList.DataTable({
        language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        ajax: {
            url: "admin/"+controller+"/data_index",
            type: 'POST',
            data: {'csrfToken': get_cookie('csrfCookie')},
        },
        order: [[1, "asc"]],
        pageLength: 10,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "name", width: "220px", className: "", orderable: true},
            {data: "email", className: "", orderable: true},
            {data: "lastLogin", width: "100px", className: "", orderable: true},
            {data: "updateDate", width: "100px", className: "", orderable: true},
            {data: "active", width: "50px", className: "text-center", orderable: false},
            {data: "action", width: "30px", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
