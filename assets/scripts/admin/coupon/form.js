
function formatAmountNoDecimals( number ) {
      var rgx = /(\d+)(\d{3})/;
      while( rgx.test( number ) ) {
          number = number.replace( rgx, '$1' + ',' + '$2' );
      }
      return number;
}

function formatAmount( number ) {

      // remove all the characters except the numeric values
      number = number.replace( /[^0-9]/g, '' );

      // set the default value
      // if( number.length == 0 ) number = "0.00";
      // else if( number.length == 1 ) number = "0.0" + number;
      // else if( number.length == 2 ) number = "0." + number;
      // else number = number.substring( 0, number.length - 2 ) + '.' + number.substring( number.length - 2, number.length );

      // // set the precision
      // number = new Number( number );
      // number = number.toFixed( 2 );    // only works with the "."

      // // change the splitter to ","
      // number = number.replace( /\./g, '.' );

      // format the amount
      x = number.split( ',' );
      x1 = x[0];
      x2 = x.length > 1 ? ',' + x[1] : '';

      return formatAmountNoDecimals( x1 ) + x2;
}
$(document).ready(function() {
     
      $('.amount').keyup( function() {
        var dd = $('input[name=type]:checked').val();
       // alert(dd);
         if(!dd){        
            $('#alert_e').html('กรุณาเลือกประเภทส่วนลด');
            $('#input-discount').val('');
            return false;
         }else{
            $(this ).val( formatAmount( $( this ).val() ) );
         }
          
      });

});
"use strict";
// Upload and image cropper
var dataTmpl = {}
var tmplName
var container
var objUploadId
var objUpload
var single
var tLang
var $image = $('#image-crop')
var cropData = {}
var options = {
    aspectRatio: 4 / 3,
    autoCropArea: 0.8,
    minContainerHeight: 480,
    dragMode: 'move'
}
var set_container = function(obj, select, tmpl, lang){
   container = $(obj)
   tmplName = tmpl
   single = true
   tLang = lang
   if ( select === 'multiple' )
       single = false
}

var remove_upload = function(obj){
    // if ( confirm('กรุณายืนยันการทำรายการ!') ) {
    //     obj.parents('div.item').remove();
    // } else {
    //     return false;
    // }

    bootbox.dialog({
      message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> กรุณายืนยันการทำรายการ</span>",
      className : "my_width",
      buttons:
      {
        "success" :
        {
          "label" : "<i class='fa fa-check'></i> ตกลง",
          "className" : "btn-sm btn-success",
          "callback": function() {
            
               obj.parents('div#action').prevAll().eq(1).remove()
            obj.parents('div#action').prevAll().eq(0).remove()
            obj.parents('div#action').remove()

            }

        },
        "cancel" :
        {
          "label" : "<i class='fa fa-times'></i> ยกเลิก",
          "className" : "btn-sm btn-white",
        }
      }
    });

}



$(document).ready(function () {

	var isCourse = document.getElementById("isCourse");
    var isCourseid = isCourse.options[isCourse.selectedIndex].value;
    if(isCourseid=='1'){
    	$('#isCourseNum-d').show(100);    
    }else{
    	$('#isCourseNum-d').hide(100);   
    } 

    var isMember = document.getElementById("isMember");
    var isMemberid = isMember.options[isMember.selectedIndex].value;
    if(isMemberid=='1'){
    	$('#isMemberNum-d').show(100);    
    }else{
    	$('#isMemberNum-d').hide(100);   
    } 
    

    $('#isCourse').change(function(obj) {
    	var e = document.getElementById("isCourse");
        var id = e.options[e.selectedIndex].value;
        if(id=='1'){
        	$('#isCourseNum-d').show(100);    
        }else{
	    	$('#isCourseNum-d').hide(100);   
	    }        
    });
    $('#isMember').change(function(obj) {
    	var e = document.getElementById("isMember");
        var id = e.options[e.selectedIndex].value;
        if(id=='1'){
        	$('#isMemberNum-d').show(100);    
        }else{
	    	$('#isMemberNum-d').hide(100);   
	    } 
              
    });
    // $('#display2').click(function(obj) {
    //     $('.discount').removeClass('amount');
    // });

    

    $('input[name="dateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $('input[name="startDate"]').val(picker.startDate.format('YYYY-MM-DD'))
        $('input[name="endDate"]').val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $('input[name="startDate"]').val('')
        $('input[name="endDate"]').val('')        
    })  
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
