"use strict";
// Upload and image cropper
var dataTmpl = {}
var tmplName
var container
var objUploadId
var objUpload
var single
var $image = $('#image-crop')
var cropData = {}
var options = {
    aspectRatio: 1 / 1,
    autoCropArea: 0.8,
    minContainerHeight: 480,
    dragMode: 'move'
}
var set_container = function(obj, select, tmpl){
   container = $(obj)
   tmplName = tmpl
   single = true
   if ( select === 'multiple' )
       single = false
}
var set_cropper = function(obj){
   objUpload = obj.parents('div#action').prevAll().eq(1).find('img')
   objUploadId = obj.parents('div#action').prevAll().eq(0).find('input#uploadId')
}
var remove_upload = function(obj){
    // if ( confirm('กรุณายืนยันการทำรายการ!') ) {
    //     obj.parents('div#action').prevAll().eq(1).remove()
    //     obj.parents('div#action').prevAll().eq(0).remove()
    //     obj.parents('div#action').remove()
    // } else {
    //     return false;
    // }


    bootbox.dialog({
      message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> กรุณายืนยันการทำรายการ</span>",
      className : "my_width",
      buttons:
      {
        "success" :
        {
          "label" : "<i class='fa fa-check'></i> ตกลง",
          "className" : "btn-sm btn-success",
          "callback": function() {
            
            obj.parents('div#action').prevAll().eq(1).remove()
            obj.parents('div#action').prevAll().eq(0).remove()
            obj.parents('div#action').remove()

            }

        },
        "cancel" :
        {
          "label" : "<i class='fa fa-times'></i> ยกเลิก",
          "className" : "btn-sm btn-white",
        }
      }
    });
}
var modal_crop = function (callback) {
    $("#modal-yes").on("click", function () {
        callback(true);
        $("#modal-crop").modal('hide');
    })
    $("#modal-no").on("click", function () {
        callback(false);
        $("#modal-crop").modal('hide');
    })
}
modal_crop(function(confirm){
  if( confirm ){
    $image.cropper('destroy')
    $.ajax({
        url: 'admin/upload/cropper',
        type: 'POST',
        data: {cropData:cropData,uploadId:objUploadId.val(),csrfToken:csrfToken},
        success: function (data) {
            $(objUpload).attr('src', data.thumbnailUrl)
            $(objUploadId).val(data.uploadId)
            $image.cropper('destroy').attr('src', data.url)
        },
        error: function () {
            toastr["error"]("พบข้อผิดพลาดด้านเทคนิค", "ระบบแอดมิน oem")
        },
        beforeSend: function () {
        }
    })
  }
})
// END upload and image cropper

$(document).ready(function () {
    
    $("#cover-image").html(tmpl("tmpl-cover-image", dataCoverImage))
    
    $('.frm-edit').validate({
        rules: {
            email: {
                remote: {
                    url: "admin/"+controller+"/check_email",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            }
        },
        messages: {
            email: {remote: 'พบอีเมล์ซ้ำในระบบ'}
        }
    });

    $('.frm-change-password').validate({
        rules: {
            oldPassword: {
                remote: {
                    url: "admin/"+controller+"/check_password",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            },
            rePassword: {equalTo: "#input-new-password"}
        },
        messages: {
            oldPassword: {remote: 'รหัสผ่านปัจจุบันไม่ถูกต้อง'},
            rePassword: {equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'}
        }
    });

    $('.select2').change(function () {
        $('.form-horizontal').validate().element('.select2')
    })
    
// Upload and image cropper
    $('tbody.files').on('click', 'button.select',  function (){
        dataTmpl[0] = {
            uploadId: $(this).attr('id'),
            thumbnailUrl: $(this).data('thumbnail'),
            url: $(this).data('url')
        }
        if ( single ) {
            $(container).html(tmpl(tmplName, dataTmpl));
        } else {
            $(container).append(tmpl(tmplName, dataTmpl));
        }
    })
    $('#modal-crop').on('shown.bs.modal', function () {
        $.get('admin/upload/get_image', {uploadId:objUploadId.val()}, function(data){
            $image.cropper('destroy').attr('src', data.url)
            $image.cropper(options)
            $image.on({
                crop: function (e) {
                    cropData.x = e.x
                    cropData.y = e.y
                    cropData.width = e.width
                    cropData.height = e.height
                    cropData.rotate = e.rotate
                    cropData.scaleX = e.scaleX
                    cropData.scaleY = e.scaleY
                }
            })
        })
    });
// END upload and image cropper


})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
