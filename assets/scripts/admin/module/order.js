"use strict";

$(document).ready(function () {
    $('#nestable').nestable({
        maxDepth: 3,
        json: category,
    }).on('change', updateOutput);

    updateOutput($('#nestable').data('output', $('#nestable-output')));

    $("#expand-all").click(function () {
        $('.dd').nestable('expandAll')
    })
    $("#collapse-all").click(function () {
        $('.dd').nestable('collapseAll')
    })

})

var updateOutput = function (e) {
    var list = e.length ? e : $(e.target),
            output = list.data('output');
    if (window.JSON) {
        output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
    } else {
        output.val('JSON browser support required for this demo.');
    }
};
$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
