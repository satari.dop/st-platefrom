"use strict";
// Upload and image cropper
var dataTmpl = {}
var tmplName
var container
var objUploadId
var objUpload
var single
var tLang
var $image = $('#image-crop')
var cropData = {}
var options = {
    aspectRatio: 4 / 3,
    autoCropArea: 0.8,
    minContainerHeight: 480,
    dragMode: 'move'
}
var set_container = function(obj, select, tmpl, lang){
   container = $(obj)
   tmplName = tmpl
   single = true
   tLang = lang
   if ( select === 'multiple' )
       single = false
}

var remove_upload = function(obj){
    // if ( confirm('กรุณายืนยันการทำรายการ!') ) {
    //     obj.parents('div.item').remove();
    // } else {
    //     return false;
    // }

    bootbox.dialog({
      message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> กรุณายืนยันการทำรายการ</span>",
      className : "my_width",
      buttons:
      {
        "success" :
        {
          "label" : "<i class='fa fa-check'></i> ตกลง",
          "className" : "btn-sm btn-success",
          "callback": function() {
            
              obj.parents('div#action').prevAll().eq(1).remove()
            obj.parents('div#action').prevAll().eq(0).remove()
            obj.parents('div#action').remove()

            }

        },
        "cancel" :
        {
          "label" : "<i class='fa fa-times'></i> ยกเลิก",
          "className" : "btn-sm btn-white",
        }
      }
    });

}

$(document).ready(function () {
    
     $("#cover-image").html(tmpl("tmpl-cover-image", dataCoverImage))
       $("#content-image").html(tmpl("tmpl-content-image", dataContentImage))
    
    // create Daterange 
    $('input[name="dateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $(this).nextAll().eq(0).val(picker.startDate.format('YYYY-MM-DD'))
        $(this).nextAll().eq(1).val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $(this).nextAll().eq(0).val('')
        $(this).nextAll().eq(1).val('')        
    }) 
    
    // เลือกไฟล์จาก upload modal
    var sum = 0 ;
    $('tbody.files').on('click', 'button.select',  function (){
        sum++;
        //var total = 
        dataTmpl[0] = {
            uploadId: $(this).attr('id'),
            thumbnailUrl: $(this).data('thumbnail'),
            url: $(this).data('url'),
            lang: tLang
        }
       
        if ( single ) {
            $(container).html(tmpl(tmplName, dataTmpl));
        } else{
            if($("#banner-image-th .item").length > 9){
                 alert_box('จำกัดแค่ 10 รูปภาพ ');
                return false;
            }else{
                // alert(sum);
                $(container).append(tmpl(tmplName, dataTmpl));
            }
        }
        $('input[name="dateRange"]').daterangepicker({
            locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
            autoUpdateInput: false,
        })   
        $('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
            $(this).nextAll().eq(0).val(picker.startDate.format('YYYY-MM-DD'))
            $(this).nextAll().eq(1).val(picker.endDate.format('YYYY-MM-DD'))
        })
        $('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
            $(this).nextAll().eq(0).val('')
            $(this).nextAll().eq(1).val('')        
        })      
    })
    
    // Drag and drop
    $( "#banner-image-th" ).sortable({
        helper: 'clone',
        placeholder: 'ui-state-highlight',
        forcePlaceholderSize: true,
        opacity: 0.6,
        axis: "y",
        cursor: 'move',
        start: function(e, ui){
            ui.placeholder.height(ui.item.height()-30);
        }
    });
    $( "#banner-image-th" ).disableSelection();
    
    
    $( "#banner-image-en" ).sortable({
        helper: 'clone',
        placeholder: 'ui-state-highlight',
        forcePlaceholderSize: true,
        opacity: 0.6,
        axis: "y",
        start: function(e, ui){
            ui.placeholder.height(ui.item.height()-30);
        }
    });
    $( "#banner-image-en" ).disableSelection();
    
})
function fixPlaceholderModified(e, ui) {
    console.log(ui)
    ui.children().each(function () {
        $(this).width($(this).width());
    });
    return ui;
}
$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
