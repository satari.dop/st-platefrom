 $(document).ready(function () {
	

    $('.registerSave').click(function(){
    	var courseId = $("#courseId").val();
    	var promotionId = $("#promotionId").val();
    	var price = $("#price").val();
      var couponCode = $("#couponCode").val();

      if(couponCode!=""){

        $('#form-error-div').html(' ');
        $('#form-success-div').html(' ');
        $.post(siteUrl+'/course/checkCoupon',{csrfToken: csrfToken,couponCode:couponCode},function(data){
        $('#form-img-div').html('<img src="'+baseUrl+'assets/images/loading1.gif" alt="loading" title="loading" style="display:inline">');
        //console.log(data);
                var res = JSON.parse(data);

                if(res.info_txt == "error")
                  {
                       $('#form-img-div').html(' ');
                       $("#form-error-div").append("<p><strong><li class='text-red'></li>"+res.msg+"</strong>,"+res.msg2+"</p>");
                       $("#form-error-div").slideDown(400);

                       return false;  
                  }

                if(res.info_txt == "success")
                  {

                    if(confirm("ยืนยันการลงทะเบียนอีกครั้ง !!")){
                      $('#form-error-div').html(' ');
                      $('#form-success-div').html(' ');

                      $.post(siteUrl+'/course/registerSave',{csrfToken: csrfToken,price:price,couponCode:couponCode,type:res.type_,discount:res.discount,courseId:courseId,promotionId:promotionId },function(data){
                           $('#form-img-div').html('<img src="'+baseUrl+'assets/images/loading1.gif" alt="loading" title="loading" style="display:inline">');
                           //console.log(data);
                           var res = JSON.parse(data);
                           if(res.info_txt=="success"){ 
                                  $('#form-img-div').html(' ');
                                 
                                  // $("#form-success-div").append('<p><strong><li class="text-green"></li>'+res.msg+'</strong>,'+res.msg2+'</p>');
                                  // $("#form-success-div").slideDown(400);
                                  // alert('ลงทะเบียนสำเร็จ');

                                  // setInterval(function(){
                                  //      window.location=siteUrl+'course/register_success/'+res.result;
                                  //     },1000
                                  // );
                                  swal({ 
                                    title: res.msg,
                                    text: "",
                                    type: "success" 
                                  }).then(function() {
                                      window.location.href = siteUrl+'course/register_success/'+res.result;
                                  })
                                      
                            } 
                            if(res.info_txt == "error")
                                    {
                                         $('#form-img-div').html(' ');
                                         $("#form-error-div").append("<p><strong><li class='text-red'></li>"+res.msg+"</strong>,"+res.msg2+"</p>");
                                         $("#form-error-div").slideDown(400);
                                        
                                    }
                          }); 
                    }else{
                      return false;
                    }

                }
        }); 

      }else{

            if(confirm("ยืนยันการลงทะเบียนอีกครั้ง !!")){
                $('#form-error-div').html(' ');
                $('#form-success-div').html(' ');
                 $('#form-img-div').html('<img src="'+baseUrl+'assets/images/loading1.gif" alt="loading" title="loading" style="display:inline">');
        
                $.post(siteUrl+'/course/registerSave',{csrfToken: csrfToken,price:price,couponCode:couponCode,courseId:courseId,promotionId:promotionId },function(data){
                     $('#form-img-div').html('<img src="'+baseUrl+'assets/images/loading1.gif" alt="loading" title="loading" style="display:inline">');
                     //console.log(data);
                     var res = JSON.parse(data);
                     if(res.info_txt=="success"){ 
                            $('#form-img-div').html(' ');
                           
                            // $("#form-success-div").append('<p><strong><li class="text-green"></li>'+res.msg+'</strong>,'+res.msg2+'</p>');
                            // $("#form-success-div").slideDown(400);
                            // alert('ลงทะเบียนสำเร็จ');

                            // setInterval(function(){
                            //      window.location=siteUrl+'course/register_success/'+res.result;
                            //     },1000
                            // );

                            swal({ 
                                    title: res.msg,
                                    text: "",
                                    type: "success" 
                                  }).then(function() {
                                      window.location.href = siteUrl+'course/register_success/'+res.result;
                                  })
                                
                      } 
                      if(res.info_txt == "error")
                              {
                                   $('#form-img-div').html(' ');
                                   $("#form-error-div").append("<p><strong><li class='text-red'></li>"+res.msg+"</strong>,"+res.msg2+"</p>");
                                   $("#form-error-div").slideDown(400);
                                  
                              }
                    }); 
              }else{
                return false;
              }

      }
    	//alert(price);
    	
		
    });

}); 