"use strict";

$(document).ready(function() {
      $('#transferDdate').datepicker({
         todayBtn: "linked",
         language: "it",
         autoclose: true,
         todayHighlight: true,
         format: 'dd/mm/yyyy' 
     });
       $(".timepicker").timepicker({
          defaultTime: 'current',
          minuteStep: 1,
          showInputs: false
        });
      $('.amount').keyup( function() {
        //alert('ff');
          $(this ).val(formatAmount($( this ).val()) );
          //alert($( this ).val());
      });

});
// alert(2);
$(document).ready(function(e) {

    $('form#frm-save').submit(function(event) {
        var dd = $('input[name=bankId]:checked').val();
         if(!dd){
           // $('#bankId').focus();
            $(".alert_bankId").html('<font color=red>* เลือกธนาคาร</font>'); 
            return false;
        }
        // if($('#fileUpload').val()==""){
        //     $('#fileUpload').focus();
        //     $(".alert_file").html('<font color=red>* เลือกไฟล์</font>'); 
        //     return false;
        // }
        if($('#transferDdate').val()==""){
            $('#transferDdate').focus();
            $(".alert_date").html('<font color=red>* กรอกวันที่</font>'); 
            return false;
        }
        if($('#transferTime').val()==""){
            $('#transferTime').focus();
            $(".alert_date").html('<font color=red>* กรอกเวลา</font>'); 
            return false;
        }
        if($('#amount').val()==""){
            $('#amount').focus();
            $(".alert_amount").html('<font color=red>* กรอกจำนวนเงิน</font>'); 
            return false;
        }
        

         $('#form-error-div').html(' ');
          $('#form-success-div').html(' ');

       // var conf = archiveFunction();
        //alert(conf);
        if(confirm("ยืนยันการแจ้งโอนอีกครั้ง !!")){


            event.preventDefault();             
            var formObj = $(this);
            var formURL = formObj.attr("action");
            var formData = new FormData(this);
            $('#form-img-div').html('<img src="'+baseUrl+'assets/images/loading1.gif" alt="loading" title="loading" style="display:inline">');
            $.ajax({
              url: formURL,
              type: 'POST',
              data:formData,
              dataType:"json",
              mimeType:"multipart/form-data",
              contentType: false,
              cache: false,
              processData:false,
              success: function(data, textStatus, jqXHR){

                 if(data.info_txt == "error")
                  {
                       $('#form-img-div').html(' ');
                       $("#form-error-div").append("<p><strong><li class='text-red'></li>"+data.msg+"</strong>,"+data.msg2+"</p>");
                       $("#form-error-div").slideDown(400);
                      
                  }
                  if(data.info_txt == "success")
                  {
                    $('#form-img-div').html(' ');
                    //$("#form-success-div").append('<p><strong><li class="text-green"></li>'+data.msg+'</strong>,'+data.msg2+'</p>');
                    //$("#form-success-div").slideDown(400);

                    // alert(data.msg);

                    // setInterval(function(){
                    //     window.location.href= siteUrl+"home";
                    //     },1000
                    // );
                    swal({ 
                      title: data.msg,
                      text: "",
                      type: "success" 
                    }).then(function() {
                        window.location.href = siteUrl;//'course/course_history';
                    })
                        
                  }
        
              },
              error: function(jqXHR, textStatus, errorThrown){
                 $('#form-img-div').html(' ');
                 $("#form-error-div").append("<p><strong><li class='text-red'></li>บันทึกข้อมูลไม่สำเร็จ</strong>,กรุณาลองใหม่อีกครั้ง!</p>");
                 $("#form-error-div").slideDown(400);
              }
        
            });
        }else{
          return false;
        }

    });

});

// function onloadCallback () {
 
//     grecaptcha.render('html_element', {
//       'sitekey' : recaptcha_sitekey,
//       'callback' : correctCaptcha,
//       'expired-callback': expiredCallback
//     });

// };
// function correctCaptcha(response) {
//     $('input[name="robot"]').val(response);
//     $('input[name="robot"]').next('span').addClass('hidden');
// };
// function expiredCallback(response) {
//     grecaptcha.reset();
//     $('input[name="robot"]').val("");
//     $('input[name="robot"]').next('span').removeClass('hidden');
// };

function formatAmountNoDecimals( number ) {
      var rgx = /(\d+)(\d{3})/;
      while( rgx.test( number ) ) {
          var number = number.replace( rgx, '$1' + ',' + '$2' );
      }
      return number;
}

function formatAmount(number) {
//
      var number = number.replace( /[^0-9]/g, '' );
//alert(number);
      var num = number.split(',');
      var num1 = num[0];
      var num2 = num.length > 1 ? ',' + num[1] : '';

      return formatAmountNoDecimals( num1 ) + num2;
}